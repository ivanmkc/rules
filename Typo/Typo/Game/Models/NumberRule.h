//
//  NumberRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Powerup.h"
#import "CharacterRule.h"

@interface NumberRule : CharacterRule

@end
