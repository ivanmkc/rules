
//
//  CharacterTile.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterTile.h"

@implementation CharacterTile

-(CharacterElement*) characterElement
{
    return (CharacterElement*) self.tileElement;
}

@end
