//
//  VowelRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-28.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface VowelRule : CharacterRule
@property BOOL isVowelsMode;

-(id)initWithVowelsMode:(BOOL) isVowelsMode andDescription:(NSString*) description;
@end
