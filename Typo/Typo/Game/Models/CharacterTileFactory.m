
//
//  CharacterTileFactory.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterTileFactory.h"
#import "CharacterElement.h"
#import "CharacterTile.h"

@implementation CharacterTileFactory
-(id) init
{
    self = [super init];
    
    if (self)
    {
        //Default to simple, all caps
        self.tileSet = CharacterTileSet_Simple_AllCaps;
    }
    
    return self;
}

//Create random music tile
-(Tile*) makeRandomTile
{
    NSArray* letterSet = [self getLettersForTileSet:self.tileSet];
    NSArray* fontSet = [self getFontsForTileSet:self.tileSet];
    
    //Roll a die to generate a random tile
    UInt32 characterStringIndex = arc4random()%(letterSet.count);
    NSString* characterString = [letterSet objectAtIndex:characterStringIndex];
    UInt32 fontStringIndex = arc4random()%(fontSet.count);
    NSString* fontString = [fontSet objectAtIndex:fontStringIndex];
    
    CharacterElement* character = [[CharacterElement alloc] initWithCharacter:characterString andTypeFace:[UIFont fontWithName:fontString size:40]];

    CharacterTile* tile = [[CharacterTile alloc] initWithTileElement:character];
    return tile;
}

- (NSArray*) getLettersForTileSet:(CharacterTileSet) tileSet
{
    NSArray* LETTERS_SIMPLE_ALLCAPS = @[@"A", @"B", @"C", @"D", @"E"];
    NSArray* LETTERS_SIMPLE_MIXEDCASE = [LETTERS_SIMPLE_ALLCAPS arrayByAddingObjectsFromArray:@[@"a", @"b", @"c", @"d", @"e"]];
    NSArray* LETTERS_MEDIUM_MIXEDCASE = [LETTERS_SIMPLE_MIXEDCASE arrayByAddingObjectsFromArray:@[@"F", @"G", @"H", @"I", @"f", @"g", @"h", @"i"]];
    NSArray* LETTERS_SET2_A = [LETTERS_MEDIUM_MIXEDCASE arrayByAddingObjectsFromArray:@[@"J", @"K", @"L", @"M", @"j", @"k", @"l", @"m"]];
    NSArray* LETTERS_SET2_B = [LETTERS_SET2_A arrayByAddingObjectsFromArray:@[@"N", @"O", @"P"]];
    NSArray* LETTERS_SET2_C = [LETTERS_SET2_B arrayByAddingObjectsFromArray:@[@"Q", @"R", @"S"]];
    
    NSArray* LETTERS_SET3_A = [NSArray arrayWithArray:LETTERS_SET2_C];
    NSArray* LETTERS_SET3_B = [LETTERS_SET3_A arrayByAddingObjectsFromArray:@[@"1", @"2", @"3", @"4", @"5"]];
    NSArray* LETTERS_SET3_C = [LETTERS_SET3_B arrayByAddingObjectsFromArray:@[@"6", @"7", @"8", @"9", @"10"]];
    
    NSArray* LETTERS_ALL_MIXEDCASE = [LETTERS_SET2_C arrayByAddingObjectsFromArray:@[@"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z"]];
    
    
    NSArray* letterSet;
    switch (self.tileSet)
    {
        case (CharacterTileSet_Simple_AllCaps):
        {
            letterSet = LETTERS_SIMPLE_ALLCAPS;
            break;
        }
        case (CharacterTileSet_Simple_MixedCase):
        {
            letterSet = LETTERS_SIMPLE_MIXEDCASE;
            break;
        }
        case (CharacterTileSet_Medium_MixedCase):
        {
            letterSet = LETTERS_MEDIUM_MIXEDCASE;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_A):
        {
            letterSet = LETTERS_SET2_A;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_B):
        {
            letterSet = LETTERS_SET2_B;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_C):
        {
            letterSet = LETTERS_SET2_C;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_A):
        {
            letterSet = LETTERS_SET3_A;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_B):
        {
            letterSet = LETTERS_SET3_B;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_C):
        {
            letterSet = LETTERS_SET3_C;
            break;
        }
    }
    
    return letterSet;
}

- (NSArray*) getFontsForTileSet:(CharacterTileSet) tileSet
{


    NSArray* LETTERS_SIMPLE_ALLCAPS = @[@"Futura-Medium"];
    NSArray* LETTERS_SIMPLE_MIXEDCASE = [LETTERS_SIMPLE_ALLCAPS arrayByAddingObjectsFromArray:@[@"Snell Roundhand"]];
    NSArray* LETTERS_MEDIUM_MIXEDCASE = [LETTERS_SIMPLE_MIXEDCASE arrayByAddingObjectsFromArray:@[@"Georgia", @"AmericanTypewriter"]];
    
    NSArray* LETTERS_SET2_A = @[@"Georgia"];
    NSArray* LETTERS_SET2_B = [LETTERS_SET2_A arrayByAddingObjectsFromArray:@[@"Avenir"]];
    NSArray* LETTERS_SET2_C = [LETTERS_SET2_B arrayByAddingObjectsFromArray:@[@"Didot", @"Courier"]];
    
    NSArray* LETTERS_SET3_A = @[@"Didot"];
    NSArray* LETTERS_SET3_B = [LETTERS_SET3_A arrayByAddingObjectsFromArray:@[@"Helvetica"]];
    NSArray* LETTERS_SET3_C = [LETTERS_SET3_B arrayByAddingObjectsFromArray:@[@"Didot", @"Courier"]];
    
    NSArray* fontSet;
    switch (self.tileSet)
    {
        case (CharacterTileSet_Simple_AllCaps):
        {
            fontSet = LETTERS_SIMPLE_ALLCAPS;
            break;
        }
        case (CharacterTileSet_Simple_MixedCase):
        {
            fontSet = LETTERS_SIMPLE_MIXEDCASE;
            break;
        }
        case (CharacterTileSet_Medium_MixedCase):
        {
            fontSet = LETTERS_MEDIUM_MIXEDCASE;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_A):
        {
            fontSet = LETTERS_SET2_A;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_B):
        {
            fontSet = LETTERS_SET2_B;
            break;
        }
        case (CharacterTileSet_LETTERS_SET2_C):
        {
            fontSet = LETTERS_SET2_C;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_A):
        {
            fontSet = LETTERS_SET3_A;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_B):
        {
            fontSet = LETTERS_SET3_B;
            break;
        }
        case (CharacterTileSet_LETTERS_SET3_C):
        {
            fontSet = LETTERS_SET3_C;
            break;
        }
    }
    
    return fontSet;
}
@end
