//
//  CharacterTile.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Tile.h"
#import "CharacterElement.h"
@interface CharacterTile : Tile
-(CharacterElement*) characterElement;
@end
