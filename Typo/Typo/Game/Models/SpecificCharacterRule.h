//
//  SpecificCharacterRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface SpecificCharacterRule : CharacterRule
@property NSString* character;
@property BOOL isCaseSensitive;

-(id)initWithCharacter:(NSString*) note andDescription:(NSString*) description;
-(id)initWithCharacter:(NSString*) note andIsCaseSensitive:(BOOL) isCaseSensitive andDescription:(NSString*) description;
@end
