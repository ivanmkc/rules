//
//  SpecificCharacterAndFontRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-03.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "SpecificCharacterRule.h"

@interface SpecificCharacterAndFontRule : SpecificCharacterRule
@property NSString* fontFamilyName;

-(id)initWithCharacter:(NSString*) character andIsCaseSensitive:(BOOL) isCaseSensitive andFontName:(NSString*) fontName    andDescription:(NSString*) description;

@end
