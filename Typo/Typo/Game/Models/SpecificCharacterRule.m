//
//  SpecificCharacterRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "SpecificCharacterRule.h"
#import "CharacterElement.h"

@implementation SpecificCharacterRule
-(id)initWithCharacter:(NSString*) note andDescription:(NSString*) description
{
    //Default to case-insensitive
    self = [self initWithCharacter:note andIsCaseSensitive:NO andDescription:description];
    
    return self;
}

-(id)initWithCharacter:(NSString*) note andIsCaseSensitive:(BOOL) isCaseSensitive andDescription:(NSString*) description
{
    self = [super initWithDescription:(NSString*) description];
    
    if (self)
    {
        self.isCaseSensitive = isCaseSensitive;
        self.character = note;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[CharacterElement class]])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        
        //Return result depending of if the rule is case-sensitive or not
        if (self.isCaseSensitive)
        {
            return ([characterElement.character compare:self.character] == NSOrderedSame);
        }
        else
        {
            return ([characterElement.character caseInsensitiveCompare:self.character] == NSOrderedSame);
        }
    }
    else
    {
        return NO;
    }
}
@end
