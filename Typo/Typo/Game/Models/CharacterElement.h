//
//  CharacterElement.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileElement.h"

@interface CharacterElement : TileElement
@property (strong, nonatomic) UIFont* font;
@property (strong, nonatomic) NSString* character;
-(id)initWithCharacter:(NSString*) character;
-(id)initWithCharacter:(NSString*) character andTypeFace:(UIFont*) typeFace;

-(BOOL) isANumber;
-(BOOL) isALetter;

typedef enum CharacterType
{
    CharacterType_Letter,
    CharacterType_Number,
    CharacterType_Symbol
} CharacterType;

-(CharacterType) getCharacterType;

@end
