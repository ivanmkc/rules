//
//  ItalicFontRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-03.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "ItalicFontRule.h"
#import "CharacterElement.h"

@implementation ItalicFontRule
-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[CharacterElement class]])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        //        NSLog(@"%@ vs %@", characterElement.font.familyName, self.fontFamilyName);
        NSRange range = [characterElement.font.familyName rangeOfString:@"italic" options:NSCaseInsensitiveSearch];
        return range.length>0;
    }
    else
    {
        return NO;
    }
}
@end
