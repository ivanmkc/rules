//
//  FontRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-23.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface FontRule : CharacterRule
@property NSString* fontFamilyName;

-(id)initWithFontName:(NSString*) fontName andDescription:(NSString*) description;
@end
