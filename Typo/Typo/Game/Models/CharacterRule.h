//
//  CharacterRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Rule.h"
#import "CharacterTile.h"

@interface CharacterRule : Rule
-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
@end
