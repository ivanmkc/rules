//
//  LetterWithDotRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-06.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface LetterWithDotRule : CharacterRule

@end
