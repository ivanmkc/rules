//
//  CharacterElement.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterElement.h"

@implementation CharacterElement
-(id)initWithCharacter:(NSString*) character
{
    UIFont* defaultTypeFace = [UIFont systemFontOfSize:12];
    return [self initWithCharacter:character andTypeFace:defaultTypeFace];
}
-(id)initWithCharacter:(NSString*) character andTypeFace:(UIFont*) typeFace
{
    self = [super self];
    
    if (self)
    {
        self.character = character;
        self.font = typeFace;
    }
    
    return self;
}

-(NSString*) information
{
    return self.character;
}

-(BOOL) isALetter
{
    NSArray* letters = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z"];
    return [letters containsObject:self.character];
}

-(BOOL) isANumber
{
    NSArray* numbers = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
    return [numbers containsObject:self.character];
}

-(CharacterType) getCharacterType
{
    if ([self isANumber])
    {
        return CharacterType_Number;
    }
    else if ([self isALetter])
    {
        return CharacterType_Letter;
    }
    else
    {
        return CharacterType_Symbol;
    }
}
@end
