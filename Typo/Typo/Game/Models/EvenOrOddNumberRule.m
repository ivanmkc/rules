//
//  EvenOrOddNumberRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "EvenOrOddNumberRule.h"

@implementation EvenOrOddNumberRule

-(id)initWithEvenMode:(BOOL) isEvenMode andDescription:(NSString*) description
{
    self = [super initWithDescription:description];
    
    if (self)
    {
        self.isEvenMode = isEvenMode;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isANumber])
    {
//        Number* number = (Number*) tile.tileElement;
        int intValue =  [tile.characterElement.character intValue];
        BOOL isEven = !(intValue % 2);
        
        //Validate according to what mode is
        if (isEven == self.isEvenMode)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
}
@end
