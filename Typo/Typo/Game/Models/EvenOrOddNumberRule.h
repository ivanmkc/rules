//
//  EvenOrOddNumberRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface EvenOrOddNumberRule : CharacterRule
@property BOOL isEvenMode;
-(id)initWithEvenMode:(BOOL) isEvenMode andDescription:(NSString*) description;
@end
