//
//  CharacterTileCell.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-22.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterTileCell.h"
#import "CharacterElement.h"

@interface CharacterTileCell()
@end

@implementation CharacterTileCell
-(void) setTile:(Tile *) tile
{
    [super setTile:tile];
    
    if ([tile.tileElement isKindOfClass:[CharacterElement class]])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        self.uiLabelName.text = tile.tileElement.information;
        self.uiLabelName.font = characterElement.font;
//        NSLog(characterElement.font.familyName);
    }
}

-(void) setTextColor:(UIColor*) color
{
    self.uiLabelName.textColor = color;
}
@end
