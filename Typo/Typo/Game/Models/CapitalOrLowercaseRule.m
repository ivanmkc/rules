//
//  CapitalOrLowercaseRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-20.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CapitalOrLowercaseRule.h"

@implementation CapitalOrLowercaseRule
-(id)initWithCapitalsMode:(BOOL) isCapitalsMode andDescription:(NSString*) description
{
    self = [super initWithDescription:description];
    
    if (self)
    {
        self.isCapitalsMode = isCapitalsMode;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        
        BOOL isCapitalLetter = ([[characterElement.character uppercaseStringWithLocale:[NSLocale currentLocale]] isEqualToString:characterElement.character]);
        
        //Validate according to what mode is
        if (isCapitalLetter==self.isCapitalsMode)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
}
@end
