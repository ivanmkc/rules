//
//  SerifRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-26.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "SerifRule.h"
#import "CharacterElement.h"

@implementation SerifRule
-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        return [self isFontSerif:characterElement.font.familyName];
    }
    else
    {
        return NO;
    }
}

-(BOOL) isFontSerif:(NSString*) fontName
{
    NSArray* serifFonts = @[@"Georgia", @"Baskerville", @"AmericanTypewriter"];
    if ([serifFonts containsObject:fontName])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
@end
