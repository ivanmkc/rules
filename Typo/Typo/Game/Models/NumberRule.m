//
//  NumberRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "NumberRule.h"
#import "CharacterElement.h"

@implementation NumberRule

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    return ([tile.characterElement isANumber]);
}
@end
