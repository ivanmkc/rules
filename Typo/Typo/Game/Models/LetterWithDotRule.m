//
//  LetterWithDotRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-06.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "LetterWithDotRule.h"

@implementation LetterWithDotRule

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        
        NSArray* lettersWithDots = @[@"i", @"j"];
        BOOL isContainsAHole = [lettersWithDots containsObject:characterElement.character];
        
        return isContainsAHole;
    }
    else
    {
        return NO;
    }
}

@end
