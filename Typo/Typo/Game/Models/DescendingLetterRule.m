//
//  DescendingLetterRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-19.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "DescendingLetterRule.h"
#import "CharacterElement.h"

@implementation DescendingLetterRule

-(id) initWithDescendingMode: (BOOL) isDescendingMode WithDescription:(NSString *)description
{
    self = [super initWithDescription:description];
    if (self)
    {
        self.isDescendingMode = isDescendingMode;
    }
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        NSString* maxString = @"A";
        NSString* minString = @"Z";
        //Get max ASCII value for unpressed tiles
        for (CharacterTile* unpressedTile in unpressedTiles)
        {
            CharacterElement* unpressedTileLetter = (CharacterElement*) unpressedTile.tileElement;
            
            //Only process letters
            if ([unpressedTileLetter isALetter])
            {
                if ([unpressedTileLetter.character compare:maxString options:NSCaseInsensitiveSearch] == NSOrderedDescending)
                {
                    maxString = unpressedTileLetter.character;
                }
                
                if ([unpressedTileLetter.character compare:minString options:NSCaseInsensitiveSearch] == NSOrderedAscending)
                {
                    minString = unpressedTileLetter.character;
                }
            }
        }
        
        //Validate according to what mode is
        if (self.isDescendingMode)
        {
            return ([characterElement.character compare:maxString options:NSCaseInsensitiveSearch] != NSOrderedAscending);
        }
        else
        {
            return ([characterElement.character compare:minString options:NSCaseInsensitiveSearch] != NSOrderedDescending);
        }
    }
    else
    {
        return NO;
    }
}
@end
