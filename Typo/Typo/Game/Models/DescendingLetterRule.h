//
//  DescendingLetterRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-19.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface DescendingLetterRule : CharacterRule
@property BOOL isDescendingMode;
-(id) initWithDescendingMode: (BOOL) isDescendingMode WithDescription:(NSString *)description;
@end
