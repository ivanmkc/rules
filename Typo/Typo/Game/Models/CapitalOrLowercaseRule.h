//
//  CapitalOrLowercaseRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-20.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"

@interface CapitalOrLowercaseRule : CharacterRule
@property BOOL isCapitalsMode;
-(id)initWithCapitalsMode:(BOOL) isCapitalsMode andDescription:(NSString*) description;
@end
