//
//  VowelRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-28.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "VowelRule.h"

@implementation VowelRule
-(id)initWithVowelsMode:(BOOL) isVowelsMode andDescription:(NSString*) description
{
    self = [super initWithDescription:description];
    
    if (self)
    {
        self.isVowelsMode = isVowelsMode;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        
        NSArray* vowels = @[@"A", @"E", @"I", @"O", @"U", @"a", @"e", @"i", @"o", @"u"];
        BOOL isVowel = [vowels containsObject:characterElement.character];
        
        //Validate according to what mode is
        if (isVowel==self.isVowelsMode)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
}
@end
