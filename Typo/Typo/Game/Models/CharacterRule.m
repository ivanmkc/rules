//
//  CharacterRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "CharacterRule.h"


@implementation CharacterRule
-(BOOL) validatesTile:(Tile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    CharacterTile * characterTile = (CharacterTile*) tile;
    return [self validatesCharacterTile:characterTile andUnpressedTiles:unpressedTiles];
}

-(BOOL) validatesCharacterTile:(CharacterTile*) characterTile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    NSLog(@"Warning: This function must be implemented by subclasses");
    if ([characterTile.characterElement isALetter])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
@end
