//
//  LetterWithHolesRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-03.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "LetterWithHolesRule.h"

@implementation LetterWithHolesRule

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.characterElement isALetter])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        
        NSArray* vowels = @[@"A", @"a", @"B", @"b", @"d", @"D", @"e", @"g", @"O", @"o", @"P", @"p", @"Q", @"q", @"R"];
        BOOL isContainsAHole = [vowels containsObject:characterElement.character];
        
        return isContainsAHole;
    }
    else
    {
        return NO;
    }
}

@end
