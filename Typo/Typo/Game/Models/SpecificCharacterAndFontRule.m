//
//  SpecificCharacterAndFontRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-03.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "SpecificCharacterAndFontRule.h"
#import "CharacterElement.h"

@implementation SpecificCharacterAndFontRule

-(id)initWithCharacter:(NSString*) character andIsCaseSensitive:(BOOL) isCaseSensitive andFontName:(NSString*) fontName    andDescription:(NSString*) description
{
    self = [super initWithCharacter:character andIsCaseSensitive:isCaseSensitive andDescription:description];
    
    if (self)
    {
        self.fontFamilyName = fontName;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[CharacterElement class]])
    {
        BOOL isCorrectCharacter = [super validatesCharacterTile:tile andUnpressedTiles:unpressedTiles];
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
        return (([characterElement.font.familyName isEqualToString:self.fontFamilyName])&&isCorrectCharacter);
    }
    else
    {
        return NO;
    }
}
@end
