//
//  FontRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-23.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FontRule.h"
#import "CharacterElement.h"

@implementation FontRule
-(id)initWithFontName:(NSString*) fontName andDescription:(NSString*) description
{
    self = [super initWithDescription:(NSString*) description];
    
    if (self)
    {
        self.fontFamilyName = fontName;
    }
    
    return self;
}

-(BOOL) validatesCharacterTile:(CharacterTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[CharacterElement class]])
    {
        CharacterElement* characterElement = (CharacterElement*) tile.tileElement;
//        NSLog(@"%@ vs %@", characterElement.font.familyName, self.fontFamilyName);
        return ([characterElement.font.familyName isEqualToString:self.fontFamilyName]);
    }
    else
    {
        return NO;
    }
}
@end
