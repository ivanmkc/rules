//
//  CharacterTileFactory.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileFactory.h"

@interface CharacterTileFactory : TileFactory
typedef enum CharacterTileSet{
    CharacterTileSet_Simple_AllCaps,
    CharacterTileSet_Simple_MixedCase,
    CharacterTileSet_Medium_MixedCase,
    CharacterTileSet_LETTERS_SET2_A,
    CharacterTileSet_LETTERS_SET2_B,
    CharacterTileSet_LETTERS_SET2_C,
    CharacterTileSet_LETTERS_SET3_A,
    CharacterTileSet_LETTERS_SET3_B,
    CharacterTileSet_LETTERS_SET3_C
} CharacterTileSet;

@end
