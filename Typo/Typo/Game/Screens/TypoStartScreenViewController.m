//
//  TypoStartScreenViewController.m
//  Typo
//
//  Created by Ivan Cheung on 2014-09-28.
//  Copyright (c) 2014 Singspiel. All rights reserved.
//

#import "TypoStartScreenViewController.h"

//Character Rules
#import "SpecificCharacterRule.h"
#import "DescendingLetterRule.h"
#import "CapitalOrLowercaseRule.h"
#import "FontRule.h"
#import "SerifRule.h"
#import "VowelRule.h"
#import "LetterWithHolesRule.h"
#import "NumberRule.h"
#import "EvenOrOddNumberRule.h"
#import "SpecificCharacterAndFontRule.h"
#import "LetterWithDotRule.h"
#import "NumberRule.h"

#import "TileSetSwitcherSection.h"
#import "TileFactoryManager.h"
#import "CharacterTileFactory.h"

@interface TypoStartScreenViewController ()

@end

@implementation TypoStartScreenViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    //Set the tile factory manager
    [TileFactoryManager setInstance:[[CharacterTileFactory alloc] init]];
    [TileFactoryManager setNibInstance:[UINib nibWithNibName:@"CharacterTileCell" bundle:[NSBundle mainBundle]]];
    
    return self;
}

//Subclasses should override this. TODO: Make this a protocol
-(int) exerciseStartIndex
{
    return 5;
}

//Override superclass to provide sections
-(NSArray*) sections
{
    if ([super sections] == nil)
    {
        //Music sections
        Rule* Rule_CharB = [[SpecificCharacterRule alloc] initWithCharacter:@"B" andDescription:@"Task: Pick B"];
        Rule* Rule_CharC = [[SpecificCharacterRule alloc] initWithCharacter:@"C" andDescription:@"Task: Pick C"];
        Rule* Rule_CharLowercaseC = [[SpecificCharacterRule alloc] initWithCharacter:@"c" andIsCaseSensitive:YES andDescription:@"Task: Pick lowercase c"];
        Rule* Rule_CharUppercaseG = [[SpecificCharacterRule alloc] initWithCharacter:@"G" andIsCaseSensitive:YES andDescription:@"Task: Pick uppercase G"];
        Rule* Rule_CharUppercaseJ = [[SpecificCharacterRule alloc] initWithCharacter:@"J" andIsCaseSensitive:YES andDescription:@"Task: Pick uppercase J"];
        Rule* Rule_LetterDescending = [[DescendingLetterRule alloc] initWithDescendingMode:YES WithDescription:@"Task: Pick descending letters"];
        Rule* Rule_LetterAscending = [[DescendingLetterRule alloc] initWithDescendingMode:NO WithDescription:@"Task: Pick ascending letters"];
        
        Rule* Rule_LetterCapitals = [[CapitalOrLowercaseRule alloc] initWithCapitalsMode:YES andDescription:@"Task: Pick uppercase letters"];
        Rule* Rule_LetterLowercase = [[CapitalOrLowercaseRule alloc] initWithCapitalsMode:NO andDescription:@"Task: Pick lowercase letters"];
        
        Rule* Rule_FontGeorgia = [[FontRule alloc] initWithFontName:@"Georgia" andDescription:@"Task: Pick font Georgia"];
        Rule* Rule_FontSnellRoundhand = [[FontRule alloc] initWithFontName:@"Snell Roundhand" andDescription:@"Task: Pick font Snell Roundhand"];
        Rule* Rule_FontFutura = [[FontRule alloc] initWithFontName:@"Futura" andDescription:@"Task: Pick font Futura"];
        Rule* Rule_FontHelvetica = [[FontRule alloc] initWithFontName:@"Helvetica" andDescription:@"Task: Pick font Helvetica"];
        Rule* Rule_FontDidot = [[FontRule alloc] initWithFontName:@"Didot" andDescription:@"Task: Pick font Didot"];
        Rule* Rule_FontAvenir = [[FontRule alloc] initWithFontName:@"Avenir" andDescription:@"Task: Pick font Avenir"];
        Rule* Rule_FontCourier = [[FontRule alloc] initWithFontName:@"Courier" andDescription:@"Task: Pick font Courier"];
        
        Rule* Rule_CharI_Futura = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"I" andIsCaseSensitive:NO andFontName:@"Futura" andDescription:@"Task: Pick I with font Futura"];
        Rule* Rule_CharF_Avenir = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"F" andIsCaseSensitive:NO andFontName:@"Avenir" andDescription:@"Task: Pick F with font Avenir"];
        Rule* Rule_CharK_Georgia = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"K" andIsCaseSensitive:NO andFontName:@"Georgia" andDescription:@"Task: Pick K with font Georgia"];
        
        Rule* Rule_Serif = [[SerifRule alloc] initWithDescription:@"Task: Pick serif fonts"];
        
        Rule* Rule_Vowels = [[VowelRule alloc] initWithVowelsMode:YES andDescription:@"Task: Pick vowels"];
        Rule* Rule_Consonants = [[VowelRule alloc] initWithVowelsMode:NO andDescription:@"Task: Pick consonants"];
        
        Rule* Rule_LetterWithHole = [[LetterWithHolesRule alloc] initWithDescription:@"Task: Pick letters with holes"];
        Rule* Rule_LetterWithDot = [[LetterWithDotRule alloc] initWithDescription:@"Task: Pick letters with a dot"];
        
        Rule* Rule_NumberRule = [[NumberRule alloc] initWithDescription:@"Task: Pick numbers"];
        Rule* Rule_Number3 = [[SpecificCharacterRule alloc] initWithCharacter:@"3" andDescription:@"Task: Pick number 3"];
        Rule* Rule_EvenNumbers = [[EvenOrOddNumberRule alloc] initWithEvenMode:YES andDescription:@"Task: Pick even numbers"];
        
        //Switch to harder letter set
        TileSetSwitcherSection* switcher0 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_Simple_AllCaps forTileFactoryClass:[CharacterTileFactory class]];
        
        //Knowledge 0
        InstructionSection* knowledge0 = [[InstructionSection alloc] initWithInstruction:@"This is your desk."];
        
        //Knowledge 1
        RulesSection* knowledge1 = [[RulesSection alloc] initWithInstruction:@"Here's your first task." andRules:@[Rule_CharB]];
        //        knowledge1.tile = [[CharacterTile alloc] initWithTileElement:[[Note alloc] initWithMidiValue:60 andBeatCount:1]];
        knowledge1.numberOfTiles = 1;
        
        //Knowledge 2
        RulesSection* knowledge2 = [[RulesSection alloc] initWithInstruction:@"Good job! Let's try something harder." andRules:@[Rule_CharC, Rule_CharB]];
        //        knowledge2.tile = [[MusicTile alloc] initWithTileElement:[[Note alloc] initWithMidiValue:62 andBeatCount:1]];
        knowledge2.numberOfTiles = 2;
        
        //Knowledge 3
        RulesSection* knowledge3 = [[RulesSection alloc] initWithInstruction:@"They did say you were top of your class." andRule:Rule_LetterDescending];
        knowledge3.numberOfTiles = 3;
        
        //Knowledge End
        InstructionSection* knowledgeEnd = [[InstructionSection alloc] initWithInstruction:@"Let's see if you can handle a real project."];
        
        //Initialize rules array
        NSMutableArray* rulesArray = [NSMutableArray arrayWithObject:Rule_LetterDescending];
        
        //Exercise 1
        RulesSection* exercise1 = [[RulesSection alloc] initWithInstruction:@"Here's your first project." andRules:rulesArray];
        exercise1.numberOfTiles = 4;
        
        //Exercise 2
        [rulesArray insertObject:Rule_CharB atIndex:0];
        RulesSection* exercise2 = [[RulesSection alloc] initWithInstruction:@"Good job. This should be easy." andRules:rulesArray];
        exercise2.numberOfTiles = 9;
        
        //        //Exercise 6
        //        Knowledge* knowledge6 = [[Knowledge alloc] initWithKnowledgeString:@"New rule: D!" andRules:@[Rule_CharC, Rule_CharA, Rule_LetterDescending]];
        //        knowledge6.numberOfTiles = 9;
        
        //Switch to harder letter set
        TileSetSwitcherSection* switcher1 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_Simple_MixedCase forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 3
        [rulesArray insertObject:Rule_LetterCapitals atIndex:0];
        RulesSection* exercise3 = [[RulesSection alloc] initWithInstruction:@"Let's try cases!" andRules:rulesArray];
        exercise3.numberOfTiles = 9;
        
        //Exercise 4
        [rulesArray insertObject:Rule_FontSnellRoundhand atIndex:0];
        RulesSection* exercise4 = [[RulesSection alloc] initWithInstruction:@"This font is Snell Roundhand!" andRules:rulesArray];
        exercise4.numberOfTiles = 9;
        
        //Exercise 5
        [rulesArray insertObject:Rule_CharLowercaseC atIndex:0];
        RulesSection* exercise5 = [[RulesSection alloc] initWithInstruction:@"Be careful of the case." andRules:rulesArray];
        exercise5.numberOfTiles = 16;
        
        //Switch to harder letter set
        TileSetSwitcherSection* switcher2 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_Medium_MixedCase forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 6
        [rulesArray insertObject:Rule_FontFutura atIndex:0];
        RulesSection* exercise6 = [[RulesSection alloc] initWithInstruction:@"This font is Futura." andRules:rulesArray];
        exercise6.numberOfTiles = 16;
        
        //Exercise 7
        [rulesArray insertObject:Rule_Vowels atIndex:0];
        RulesSection* exercise7 = [[RulesSection alloc] initWithInstruction:@"Everyone loves vowels." andRules:rulesArray];
        exercise7.numberOfTiles = 16;
        
        //Exercise 8
        [rulesArray insertObject:Rule_FontGeorgia atIndex:0];
        RulesSection* exercise8 = [[RulesSection alloc] initWithInstruction:@"This font is Georgia." andRules:rulesArray];
        exercise8.numberOfTiles = 16;
        
        //Exercise 9
        [rulesArray insertObject:Rule_CharI_Futura atIndex:0];
        RulesSection* exercise9 = [[RulesSection alloc] initWithInstruction:@"Mind the font!" andRules:rulesArray];
        exercise9.numberOfTiles = 16;
        
        //Exercise 10
        [rulesArray insertObject:Rule_CharUppercaseG atIndex:0];
        RulesSection* exercise10 = [[RulesSection alloc] initWithInstruction:@"Uppercase G's are my favorite." andRules:rulesArray];
        exercise10.numberOfTiles = 16;
        
        //New project group
        //Switch letter set
        TileSetSwitcherSection* switcher3 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_LETTERS_SET2_A forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 11
        //Reset rules array
        rulesArray = [NSMutableArray arrayWithObject:Rule_LetterAscending];
        RulesSection* exercise11 = [[RulesSection alloc] initWithInstruction:@"Time to go up!" andRules:rulesArray];
        exercise11.numberOfTiles = 16;
        
        //Exercise 12
        [rulesArray insertObject:Rule_LetterLowercase atIndex:0];
        RulesSection* exercise12 = [[RulesSection alloc] initWithInstruction:@"Keep it lowercase." andRules:rulesArray];
        exercise12.numberOfTiles = 16;
        
        //Switch letter set
        TileSetSwitcherSection* switcher4 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_LETTERS_SET2_B forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 13
        [rulesArray insertObject:Rule_FontAvenir atIndex:0];
        RulesSection* exercise13 = [[RulesSection alloc] initWithInstruction:@"That is one sleek font." andRules:rulesArray];
        exercise13.numberOfTiles = 16;
        
        //Exercise 14
        [rulesArray insertObject:Rule_LetterWithHole atIndex:0];
        RulesSection* exercise14 = [[RulesSection alloc] initWithInstruction:@"Hole in one." andRules:rulesArray];
        exercise14.numberOfTiles = 16;
        
        //        //Exercise 15
        //        [rulesArray insertObject:Rule_Consonants atIndex:0];
        //        InstructionAndRuleSection* exercise15 = [[InstructionAndRuleSection alloc] initWithInstruction:@"Not vowels this time." andRules:rulesArray];
        //        exercise15.numberOfTiles = 16;
        
        //Exercise 15
        [rulesArray insertObject:Rule_CharF_Avenir atIndex:0];
        RulesSection* exercise15 = [[RulesSection alloc] initWithInstruction:@"Fvenir." andRules:rulesArray];
        exercise15.numberOfTiles = 16;
        
        //Exercise 16
        [rulesArray insertObject:Rule_CharUppercaseJ atIndex:0];
        RulesSection* exercise16 = [[RulesSection alloc] initWithInstruction:@"J. Just J." andRules:rulesArray];
        exercise16.numberOfTiles = 16;
        
        //Switch letter set
        TileSetSwitcherSection* switcher5 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_LETTERS_SET2_C forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 17
        [rulesArray insertObject:Rule_FontDidot atIndex:0];
        RulesSection* exercise17 = [[RulesSection alloc] initWithInstruction:@"The definition of chic." andRules:rulesArray];
        exercise17.numberOfTiles = 16;
        
        //Exercise 18
        [rulesArray insertObject:Rule_CharK_Georgia atIndex:0];
        RulesSection* exercise18 = [[RulesSection alloc] initWithInstruction:@"K in font Georgia." andRules:rulesArray];
        exercise18.numberOfTiles = 16;
        
        //Exercise 19
        [rulesArray insertObject:Rule_LetterWithDot atIndex:0];
        RulesSection* exercise19 = [[RulesSection alloc] initWithInstruction:@"Dot the _'s." andRules:rulesArray];
        exercise19.numberOfTiles = 16;
        
        //Exercise 20
        [rulesArray insertObject:Rule_FontCourier atIndex:0];
        RulesSection* exercise20 = [[RulesSection alloc] initWithInstruction:@"You've seen this before." andRules:rulesArray];
        exercise20.numberOfTiles = 16;
        
        //Switch to harder letter set
        TileSetSwitcherSection* switcher6 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_LETTERS_SET3_A forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 21
        rulesArray = [NSMutableArray arrayWithObject:Rule_LetterDescending];
        RulesSection* exercise21 = [[RulesSection alloc] initWithInstruction:@"Descending tiles!" andRules:rulesArray];
        exercise21.numberOfTiles = 16;
        
        //Switch to harder letter set
        TileSetSwitcherSection* switcher7 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_LETTERS_SET3_B forTileFactoryClass:[CharacterTileFactory class]];
        
        //Exercise 22
        [rulesArray insertObject:Rule_NumberRule atIndex:0];
        RulesSection* exercise22 = [[RulesSection alloc] initWithInstruction:@"Numbers!" andRules:rulesArray];
        exercise22.numberOfTiles = 16;
        
        //Exercise 23
        [rulesArray insertObject:Rule_FontDidot atIndex:0];
        RulesSection* exercise23 = [[RulesSection alloc] initWithInstruction:@"Didot part 2." andRules:rulesArray];
        exercise23.numberOfTiles = 16;
        
        //Exercise 24
        [rulesArray insertObject:Rule_Number3 atIndex:0];
        RulesSection* exercise24 = [[RulesSection alloc] initWithInstruction:@"Number 3." andRules:rulesArray];
        exercise24.numberOfTiles = 16;
        
        //Exercise 25
        [rulesArray insertObject:Rule_EvenNumbers atIndex:0];
        RulesSection* exercise25 = [[RulesSection alloc] initWithInstruction:@"Odd and even." andRules:rulesArray];
        exercise25.numberOfTiles = 16;
        
        //Exercise 26
        [rulesArray insertObject:Rule_EvenNumbers atIndex:0];
        RulesSection* exercise26 = [[RulesSection alloc] initWithInstruction:@"Odd and even." andRules:rulesArray];
        exercise26.numberOfTiles = 16;
        
        
        //
        //        //Exercise 7
        //        Knowledge* knowledge7 = [[Knowledge alloc] initWithKnowledgeString:@"New rule: Lines!" andRules:@[Rule_NoteLines, Rule_NoteD, Rule_NoteC, Rule_NoteDescending]];
        //        knowledge7.numberOfTiles = 9;
        
        //        //Character sections
        //        Knowledge* knowledge8 = [[Knowledge alloc] initWithKnowledgeString:@"New rule: Pick C" andRule:Rule_CharA];
        //        knowledge8.numberOfTiles = 9;
        
        NSMutableArray* sectionsMutable = [NSMutableArray array];
        [sectionsMutable addObject:knowledge0];
        [sectionsMutable addObject:knowledge1];
        [sectionsMutable addObject:knowledge2];
        [sectionsMutable addObject:knowledge3];
        [sectionsMutable addObject:knowledgeEnd];
        
        [sectionsMutable addObject:switcher0];
        [sectionsMutable addObject:exercise1];
        [sectionsMutable addObject:exercise2];
        [sectionsMutable addObject:switcher1];
        [sectionsMutable addObject:exercise3];
        [sectionsMutable addObject:exercise4];
        [sectionsMutable addObject:exercise5];
        [sectionsMutable addObject:switcher2];
        [sectionsMutable addObject:exercise6];
        [sectionsMutable addObject:exercise7];
        [sectionsMutable addObject:exercise8];
        [sectionsMutable addObject:exercise9];
        [sectionsMutable addObject:exercise10];
        
        [sectionsMutable addObject:switcher3];
        [sectionsMutable addObject:exercise11];
        [sectionsMutable addObject:exercise12];
        [sectionsMutable addObject:switcher4];
        [sectionsMutable addObject:exercise13];
        [sectionsMutable addObject:exercise14];
        [sectionsMutable addObject:exercise15];
        [sectionsMutable addObject:exercise16];
        [sectionsMutable addObject:switcher5];
        [sectionsMutable addObject:exercise17];
        [sectionsMutable addObject:exercise18];
        [sectionsMutable addObject:exercise19];
        [sectionsMutable addObject:exercise20];
        
        
        [sectionsMutable addObject:switcher6];
        [sectionsMutable addObject:exercise21];
        [sectionsMutable addObject:switcher7];
        [sectionsMutable addObject:exercise22];
        [sectionsMutable addObject:exercise23];
        [sectionsMutable addObject:exercise24];
        [sectionsMutable addObject:exercise25];
        
        [super setSections:[NSArray arrayWithArray:sectionsMutable]];
    }
    
    return [super sections];
}

@end
