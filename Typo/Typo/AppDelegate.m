//
//  AppDelegate.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

#define MIXPANEL_TOKEN @"c1ebfac5224528a1c4350f5aa0c5e6db"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // Mixpanel
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    mixpanel.showSurveyOnActive = NO;
    
    //Increment launch number on MixPanel
    [mixpanel.people increment:@{
                                 @"Times App Has Launched": @1
                                 }];
    
    // Detect first launch
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) {
        //Set up link between this app instance to the user
        [mixpanel identify:mixpanel.distinctId];
        [mixpanel.people setOnce:@{@"Mix ID" : mixpanel.distinctId}];
        
        //Send date to MixPanel
        NSDate* currentDate = [NSDate date];
        [mixpanel.people setOnce:@{@"First Seen On" : currentDate}];
        [mixpanel.people set:@{@"Logged Into Facebook" : @NO}];
        //Save locally
        [[NSUserDefaults standardUserDefaults] setValue:currentDate forKey:@"FirstSeenOn"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
