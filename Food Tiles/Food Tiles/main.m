//
//  main.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-26.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
