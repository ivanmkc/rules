//
//  CharacterCardView.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-12-03.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "CharacterCardView.h"
#import "CharacterCardTileCell.h"
#import "TileFactoryManager.h"
#import "FoodTileFactory.h"

@interface CharacterCardView()
@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewCharacterCard;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelCharacterName;
@property (weak, nonatomic) IBOutlet UICollectionView *uiCollectionViewTiles;
@property (strong, nonatomic) NSArray *tiles;

@property (nonatomic) int cellWidth;
@end

static const int SPACE_BETWEEN_CELLS = 10;
static const int kNumberOfTiles = 3;

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@implementation CharacterCardView
-(void) awakeFromNib
{
    self.tiles = [NSArray array];
    
    //Register nibs
    [self.uiCollectionViewTiles registerNib:[UINib nibWithNibName:@"CharacterCardTileCell" bundle:[NSBundle mainBundle]]
                forCellWithReuseIdentifier:@"TileCell"];
    
    if (IPAD)
    {
        self.uiLabelCharacterName.font = [self.uiLabelCharacterName.font fontWithSize:self.uiLabelCharacterName.font.pointSize * 2 ];
    }
}

-(void) setRule:(FoodRule *)rule
{
    _rule = rule;
    
    //Set character
    self.uiImageViewCharacterCard.image = rule.image;
    self.uiLabelCharacterName.text = [NSString stringWithFormat:@"The %@", rule.characterName];
    
    //Set tiles
    TileFactory* tileFactory = [TileFactoryManager getInstance];
    NSMutableArray* tilesMutable = [NSMutableArray arrayWithArray:[tileFactory makeTiles:kNumberOfTiles*10 withRule:rule]];
    
    //Remove duplicate tiles
    for (int tileIndex = 0; tileIndex < tilesMutable.count; tileIndex++)
    {
        Tile* tile = tilesMutable[tileIndex];

        //Remove all other instances
        for (int tileIndex2 = tilesMutable.count - 1; tileIndex2 >= tileIndex+1; tileIndex2--)
        {
            Tile* tileToRemove = tilesMutable[tileIndex2];
            if ([tileToRemove.tileElement isEqual:tile.tileElement])
            {
                [tilesMutable removeObjectAtIndex:tileIndex2];
            }
        }
    }
    self.tiles = [NSArray arrayWithArray:tilesMutable];
    
    [self.uiCollectionViewTiles reloadData];
}

#pragma mark - Collection view delegate functions
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return fmin(self.tiles.count, kNumberOfTiles);
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellIdentifier = @"TileCell";
    CharacterCardTileCell *cell = nil;

    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.tile = self.tiles[indexPath.item];
    
    cell.autoresizingMask = UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin;
//
//    Powerup* powerup = [self.powerups objectAtIndex:indexPath.item];
//    
//    [cell setPowerup:powerup];
//    
//    //    BOOL isPressed = [self.pressedTilesIndexPaths containsObject:indexPath];
//    //    [cell setIsPressed:isPressed];
//    
//    //Fire delegate function
//    //    if ([self.secondDelegate respondsToSelector:@selector(onTileCreated:forTile:)])
//    //    {
//    //        [self.secondDelegate onTileCreated:cell forTile:tile];
//    //    }
//    
//    return cell;
    return cell;
}

#pragma mark - FlowLayout delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float totalHeight = collectionView.bounds.size.height;
    float totalWidth = collectionView.bounds.size.width;
    
    float numberOfCells = fmin(self.tiles.count, kNumberOfTiles);
    
    float maxCellHeight = totalHeight;
    float maxCellWidth = totalWidth / numberOfCells - (numberOfCells - 1) * SPACE_BETWEEN_CELLS;
    
    float cellSize = fmin( maxCellHeight, maxCellWidth );
    
    //Save cell width
    self.cellWidth = cellSize;
    
    return CGSizeMake( cellSize, cellSize );
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    float totalWidth = self.uiCollectionViewTiles.frame.size.width;
    
    UICollectionViewFlowLayout * layout = (UICollectionViewFlowLayout *) collectionViewLayout;
    float spacing = layout.minimumInteritemSpacing;
    
    int numberOfCells = (int) [collectionView numberOfItemsInSection: section];
    
    float margin = (totalWidth - numberOfCells * self.cellWidth - (numberOfCells - 1)   * spacing ) / 2;
    
    return UIEdgeInsetsMake(0, margin, 0, margin);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return SPACE_BETWEEN_CELLS;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return SPACE_BETWEEN_CELLS;
}
@end
