//
//  SpecificFood.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-29.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "SpecificFood.h"

@implementation SpecificFood
-(id)initWithFood:(NSString*) food andDescription:(NSString*) description;
{
    self = [super initWithDescription:description];
    
    if (self)
    {
        self.food = food;
    }
    return self;
}

-(BOOL) validatesFoodTile:(FoodTile*) foodTile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([foodTile.foodElement.food isEqualToString:self.food])
    {
        return YES;
    }
    
    return NO;
}
@end
