//
//  SpecificTypeAndAttributesRuleFoodRule.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-28.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "FoodRule.h"

@interface SpecificTypeAndAttributeRuleFoodRule : FoodRule
@property FoodType foodType;
@property NSString* attribute;

-(id)initWithType:(FoodType) foodType andAttribute:(NSString*) attribute andDescription:(NSString*) description;
@end
