//
//  FoodExerciseScreenViewController.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-10-03.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "FoodExerciseScreenViewController.h"
#import "FoodRule.h"
#import "CharacterCardView.h"

@interface FoodExerciseScreenViewController ()
@property (weak, nonatomic) IBOutlet UIView *uiViewCharacterCardContainer;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageSpeechBubble;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageRule;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewLandscape;
@property (weak, nonatomic) IBOutlet UIView *uiViewLandscapeScreen;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewBannerOverlay;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsLayoutConstraintTopInstructionFrame;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsLayoutConstraintInstructionAndCollectionViewVerticalSpace;
@property (nonatomic) BOOL isAnimationSequenceFinished;

@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewGrid;
@property (weak, nonatomic) IBOutlet TileCollectionView *uiCollectionViewTiles;

@property (nonatomic) float originalNSLayoutConstraintTopInstructionFrameConstant;
@property (nonatomic) float originalNSLayoutConstraintInstructionAndCollectionViewVerticalSpaceConstant;

@property (strong, nonatomic) CharacterCardView* characterCardView;
@property (strong, nonatomic) UIImage* currentCharacterImage;
@end

@implementation FoodExerciseScreenViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    self.isAnimationSequenceFinished = YES;
    self.originalNSLayoutConstraintTopInstructionFrameConstant = self.nsLayoutConstraintTopInstructionFrame.constant;
    self.originalNSLayoutConstraintInstructionAndCollectionViewVerticalSpaceConstant = self.nsLayoutConstraintInstructionAndCollectionViewVerticalSpace.constant;
    
//        self.nsLayoutConstraintTopInstructionFrame.constant = 100.0f;
    self.characterCardView = [[[NSBundle mainBundle] loadNibNamed:@"CharacterCardView" owner:self options:nil] firstObject];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.view layoutSubviews];
}

-(void)prepareExerciseTutorial
{
    [super prepareExerciseTutorial];
    
    //Hide unneeded banner elements
    self.uiViewLandscapeScreen.alpha = 0.0;
    self.uiLabelInstructions.alpha = 0.0;
    self.uiLabelRule.alpha = 0.0;
    self.uiImageViewBannerOverlay.alpha = 0.0;
    //Set label to show character name
    //    self.uiLabelInstruction.text = ).characterName;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //Create new character card
    dispatch_async(dispatch_get_main_queue(), ^{
        //        self.characterCardView.hidden = YES;
        self.characterCardView.frame = self.uiViewCharacterCardContainer.bounds;
        self.characterCardView.rule = (FoodRule*) [self.uiTileCollectionView getCurrentRule];
        
        [self.uiViewCharacterCardContainer addSubview:self.characterCardView];
        
        self.uiViewCharacterCardContainer.transform = CGAffineTransformMakeTranslation(-self.view.bounds.size.width,0);
        [UIView animateWithDuration:0.5
                              delay:0
             usingSpringWithDamping:0.5
              initialSpringVelocity:20
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.uiViewCharacterCardContainer.transform = CGAffineTransformIdentity;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
    });
}

-(void)showTutorial
{
    [super showTutorial];
    
    //Show new character card
    self.uiViewCharacterCardContainer.hidden = NO;
    
    //Slowly scroll up landscape
    [UIView animateWithDuration:5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.uiImageViewLandscape.transform = CGAffineTransformMakeTranslation(0,-50);
                     }
                     completion:nil];
    
    //Move instructions down
//    self.uiImageViewGrid.hidden = YES;
}

- (void)restartExercise
{
    //Hide character card and start exercise
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.uiViewCharacterCardContainer.transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width,0);
                         self.uiViewLandscapeScreen.alpha = 1.0;
                         self.uiLabelInstructions.alpha = 1;
                        self.uiLabelRule.alpha = 1;
                         self.uiImageViewBannerOverlay.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         //Show new character card
                         self.uiViewCharacterCardContainer.hidden = YES;
                         [super restartExercise];
                     }];
}

#pragma mark - TileCollectionView delegates
- (void) onRuleReplaced:(Rule*)oldRule byNewRule:(Rule*) newRule
{
    [super onRuleReplaced:oldRule byNewRule:newRule];
    
    //Set label to show character name
    self.uiLabelInstructions.text = [NSString stringWithFormat:@"The %@", ((FoodRule*)newRule).characterName];

    if (oldRule != nil)
    {
        self.uiLabelRule.hidden = YES;
        //Hide character card and start exercise
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.uiLabelInstructions.frame = self.uiLabelRule.frame;
                         }
                         completion:^(BOOL finished) {
                         }];
    }
    
    FoodRule* foodRule = (FoodRule*) newRule;
    
    //Set the first image
    if (self.uiImageRule.image == nil)
    {
        self.uiImageRule.image = foodRule.image;
    }
    
    if (oldRule != nil)
    {
        self.currentCharacterImage = foodRule.image;
        
        if (self.isAnimationSequenceFinished)
        {
            self.isAnimationSequenceFinished = NO;
            
            //Save centerpoint
            CGPoint centerPoint = CGPointMake(self.uiImageSpeechBubble.bounds.size.width/2, self.uiImageSpeechBubble.bounds.size.height/2);
            
            //Move character down
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 self.uiImageRule.transform = CGAffineTransformMakeTranslation(-self.uiImageRule.bounds.size.width,0);
                                 

                                 self.uiImageSpeechBubble.layer.anchorPoint = CGPointMake(0, 0);
                                 
//                                 self.uiImageSpeechBubble.transform = CGAffineTransformMakeTranslation(-anchorPoint.x, -anchorPoint.y);
                                 self.uiImageSpeechBubble.transform = CGAffineTransformMakeScale(0, 0);
                                 
//                                 self.uiLabelRule.transform = CGAffineTransformMakeScale(0, 0);
//                                 self.uiLabelRule.hidden = YES;
                             }
                             completion:^(BOOL isAnimated){
//                                     if (!self.isAnimationSequenceFinished)
//                                     {
                                         self.uiImageRule.image = self.currentCharacterImage;
//                                     }
                                 
                                     // Bring it back up with springiness
                                     [UIView animateWithDuration:0.3 delay:0
                                          usingSpringWithDamping:0.45 initialSpringVelocity:20.0f
                                                         options:0 animations:^{
                                                             self.uiImageRule.transform = CGAffineTransformMakeTranslation(0, 0);
    //                                                         [self.view layoutIfNeeded];
                                                         } completion:^(BOOL isAnimated)
                                      {
                                          // Bring speech bubble back up
                                          [UIView animateWithDuration:0.2 delay:0
                                                              options:UIViewAnimationOptionCurveEaseOut animations:^{
                                                                  self.uiImageSpeechBubble.transform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(-centerPoint.x, -centerPoint.y), CGAffineTransformIdentity);
                                                                  
    //                                                              self.uiLabelRule.transform = CGAffineTransformIdentity;
    //                                                              [self.view layoutIfNeeded];
                                                              } completion:^(BOOL isAnimated)
                                           {
                                               self.isAnimationSequenceFinished = YES;
    //                                           self.uiLabelRule.hidden = NO;
                                           }];
                                      }];
                             }];
        }
        else
        {
            //Remove animations
//            [self.uiImageRule.layer removeAllAnimations];
            
//            self.uiImageRule.image = foodRule.image;
            self.currentCharacterImage = foodRule.image;
            self.isAnimationSequenceFinished = YES;
        }
    }
}

//Getter for project title
-(NSString*) projectTitle
{
    return [@"Level " stringByAppendingString:[@(self.projectNumber) stringValue]];
}
@end
