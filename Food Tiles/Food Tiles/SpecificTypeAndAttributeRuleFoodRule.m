//
//  SpecificTypeAndAttributesRuleFoodRule.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-28.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "SpecificTypeAndAttributeRuleFoodRule.h"

@implementation SpecificTypeAndAttributeRuleFoodRule
-(id)initWithType:(FoodType) foodType andAttribute:(NSString*) attribute andDescription:(NSString*) description
{
    self = [super initWithDescription:description];
    
    if (self)
    {
        self.foodType = foodType;
        self.attribute = attribute;
    }
    return self;
}

-(BOOL) validatesFoodTile:(FoodTile*) foodTile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if (foodTile.foodElement.type == self.foodType)
    {
        if ((self.attribute == nil)||([self.attribute isEqualToString:@""]))
        {
            return YES;
        }
        else if ([foodTile.foodElement.attributes containsObject:self.attribute])
        {
            return YES;
        }
    }
        
    return NO;
}
@end
