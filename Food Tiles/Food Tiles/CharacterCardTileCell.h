//
//  CharacterCardTileCellCollectionViewCell.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-12-06.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tile.h"

@interface CharacterCardTileCell : UICollectionViewCell

@property (strong, nonatomic) Tile* tile;
@end
