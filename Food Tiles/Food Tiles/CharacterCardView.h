//
//  CharacterCardView.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-12-03.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodRule.h"

@interface CharacterCardView : UIView

@property (weak, nonatomic) FoodRule* rule;

@end
