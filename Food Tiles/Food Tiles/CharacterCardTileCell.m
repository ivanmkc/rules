//
//  CharacterCardTileCellCollectionViewCell.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-12-06.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "CharacterCardTileCell.h"
#import "FoodElement.h"

@interface CharacterCardTileCell()
@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewTile;
@end

@implementation CharacterCardTileCell

- (void)awakeFromNib {
    // Initialization code
}

-(void) setTile:(Tile *) tile
{
    _tile = tile;
    
    if ([tile.tileElement isKindOfClass:[FoodElement class]])
    {
        FoodElement* foodElement = (FoodElement*) tile.tileElement;
//        self.uiLabelName.text = tile.tileElement.information;
        NSString* imageString = [NSString stringWithFormat:@"food_%@", [foodElement.food lowercaseString]];
        UIImage* fruitImage = [UIImage imageNamed:imageString];
        
        if (fruitImage != nil)
        {
            self.uiImageViewTile.image = fruitImage;
            self.uiImageViewTile.hidden = NO;
//            self.uiLabelName.hidden = YES;
        }
        else
        {
            self.uiImageViewTile.hidden = YES;
//            self.uiLabelName.hidden = NO;
        }
    }
}

@end
