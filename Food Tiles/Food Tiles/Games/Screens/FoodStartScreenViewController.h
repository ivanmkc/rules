//
//  FoodStartScreenViewController.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-28.
//  Copyright (c) 2014 Singspiel. All rights reserved.
//

#import "StartScreenViewController.h"

@interface FoodStartScreenViewController : StartScreenViewController

@end
