//
//  FoodStartScreenViewController.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-28.
//  Copyright (c) 2014 Singspiel. All rights reserved.
//

#import "FoodStartScreenViewController.h"

//Food Rules
#import "SpecificTypeAndAttributeRuleFoodRule.h"
#import "SpecificFood.h"

#import "TileSetSwitcherSection.h"
#import "TileFactoryManager.h"
#import "FoodTileFactory.h"

@interface FoodStartScreenViewController ()

@end

@implementation FoodStartScreenViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    //Set the tile factory manager
    [TileFactoryManager setInstance:[[FoodTileFactory alloc] init]];
    [TileFactoryManager setNibInstance:[UINib nibWithNibName:@"FoodTileCell" bundle:[NSBundle mainBundle]]];
    return self;
}

//Subclasses should override this. TODO: Make this a protocol
-(int) exerciseStartIndex
{
    return 0;
}

//Subclasses should override this. TODO: Make this a protocol
-(NSArray*) sections
{
    if ([super sections] == nil)
    {
        
        FoodRule* Rule_CitrusFruit = [[SpecificTypeAndAttributeRuleFoodRule alloc] initWithType:FoodType_FRUIT andAttribute:@"Citrus" andDescription:@"Citrus fruits are my thing."];
        Rule_CitrusFruit.characterName = @"Prince";
        
        FoodRule* Rule_Fruits = [[SpecificTypeAndAttributeRuleFoodRule alloc] initWithType:FoodType_FRUIT andAttribute:nil andDescription:@"Feed me any fruit!"];
        Rule_Fruits.characterName = @"Princess";
        
        FoodRule* Rule_RedFruits = [[SpecificTypeAndAttributeRuleFoodRule alloc] initWithType:FoodType_FRUIT andAttribute:@"Red" andDescription:@"Red Fruits! Give them!"];
        Rule_RedFruits.characterName = @"Red Knight";
        
        FoodRule* Rule_Strawberry = [[SpecificFood alloc] initWithFood:@"Strawberry" andDescription:@"Any strawberries left?"];
        
        FoodRule* Rule_Apple= [[SpecificFood alloc] initWithFood:@"Apple" andDescription:@"Juicy, juicy apples..."];
        Rule_Apple.characterName = @"Witch";
        
        FoodRule* Rule_Veggies = [[SpecificTypeAndAttributeRuleFoodRule alloc] initWithType:FoodType_VEGETABLE andAttribute:nil andDescription:@"Only veggies for this king!"];
        Rule_Veggies.characterName = @"King";
        
        FoodRule* Rule_Carrot = [[SpecificFood alloc] initWithFood:@"Carrot" andDescription:@"Carrots!"];
        Rule_Carrot.characterName = @"Knight";
        
        FoodRule* Rule_GreenVeggies = [[SpecificTypeAndAttributeRuleFoodRule alloc] initWithType:FoodType_VEGETABLE andAttribute:@"Green" andDescription:@"Green vegetables!"];
        Rule_GreenVeggies.characterName = @"Dwarf";
        
        FoodRule* Rule_Banana = [[SpecificFood alloc] initWithFood:@"Banana" andDescription:@"You didn't know dragons love bananas?"];
        Rule_Banana.characterName = @"Dragon";
        
        //Music sections
//        Rule* Rule_CharB = [[SpecificCharacterRule alloc] initWithCharacter:@"B" andDescription:@"Task: Pick B"];
//        Rule* Rule_CharC = [[SpecificCharacterRule alloc] initWithCharacter:@"C" andDescription:@"Task: Pick C"];
//        Rule* Rule_CharLowercaseC = [[SpecificCharacterRule alloc] initWithCharacter:@"c" andIsCaseSensitive:YES andDescription:@"Task: Pick lowercase c"];
//        Rule* Rule_CharUppercaseG = [[SpecificCharacterRule alloc] initWithCharacter:@"G" andIsCaseSensitive:YES andDescription:@"Task: Pick uppercase G"];
//        Rule* Rule_CharUppercaseJ = [[SpecificCharacterRule alloc] initWithCharacter:@"J" andIsCaseSensitive:YES andDescription:@"Task: Pick uppercase J"];
//        Rule* Rule_LetterDescending = [[DescendingLetterRule alloc] initWithDescendingMode:YES WithDescription:@"Task: Pick descending letters"];
//        Rule* Rule_LetterAscending = [[DescendingLetterRule alloc] initWithDescendingMode:NO WithDescription:@"Task: Pick ascending letters"];
//        
//        Rule* Rule_LetterCapitals = [[CapitalOrLowercaseRule alloc] initWithCapitalsMode:YES andDescription:@"Task: Pick uppercase letters"];
//        Rule* Rule_LetterLowercase = [[CapitalOrLowercaseRule alloc] initWithCapitalsMode:NO andDescription:@"Task: Pick lowercase letters"];
//        
//        Rule* Rule_FontGeorgia = [[FontRule alloc] initWithFontName:@"Georgia" andDescription:@"Task: Pick font Georgia"];
//        Rule* Rule_FontSnellRoundhand = [[FontRule alloc] initWithFontName:@"Snell Roundhand" andDescription:@"Task: Pick font Snell Roundhand"];
//        Rule* Rule_FontFutura = [[FontRule alloc] initWithFontName:@"Futura" andDescription:@"Task: Pick font Futura"];
//        Rule* Rule_FontHelvetica = [[FontRule alloc] initWithFontName:@"Helvetica" andDescription:@"Task: Pick font Helvetica"];
//        Rule* Rule_FontDidot = [[FontRule alloc] initWithFontName:@"Didot" andDescription:@"Task: Pick font Didot"];
//        Rule* Rule_FontAvenir = [[FontRule alloc] initWithFontName:@"Avenir" andDescription:@"Task: Pick font Avenir"];
//        Rule* Rule_FontCourier = [[FontRule alloc] initWithFontName:@"Courier" andDescription:@"Task: Pick font Courier"];
//        
//        Rule* Rule_CharI_Futura = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"I" andIsCaseSensitive:NO andFontName:@"Futura" andDescription:@"Task: Pick I with font Futura"];
//        Rule* Rule_CharF_Avenir = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"F" andIsCaseSensitive:NO andFontName:@"Avenir" andDescription:@"Task: Pick F with font Avenir"];
//        Rule* Rule_CharK_Georgia = [[SpecificCharacterAndFontRule alloc] initWithCharacter:@"K" andIsCaseSensitive:NO andFontName:@"Georgia" andDescription:@"Task: Pick K with font Georgia"];
//        
//        Rule* Rule_Serif = [[SerifRule alloc] initWithDescription:@"Task: Pick serif fonts"];
//        
//        Rule* Rule_Vowels = [[VowelRule alloc] initWithVowelsMode:YES andDescription:@"Task: Pick vowels"];
//        Rule* Rule_Consonants = [[VowelRule alloc] initWithVowelsMode:NO andDescription:@"Task: Pick consonants"];
//        
//        Rule* Rule_LetterWithHole = [[LetterWithHolesRule alloc] initWithDescription:@"Task: Pick letters with holes"];
//        Rule* Rule_LetterWithDot = [[LetterWithDotRule alloc] initWithDescription:@"Task: Pick letters with a dot"];
//        
//        Rule* Rule_NumberRule = [[NumberRule alloc] initWithDescription:@"Task: Pick numbers"];
//        Rule* Rule_Number3 = [[SpecificCharacterRule alloc] initWithCharacter:@"3" andDescription:@"Task: Pick number 3"];
//        Rule* Rule_EvenNumbers = [[EvenOrOddNumberRule alloc] initWithEvenMode:YES andDescription:@"Task: Pick even numbers"];
//        
//        //Switch to harder letter set
//        TileSetSwitcherSection* switcher0 = [[TileSetSwitcherSection alloc] initWithTargetTileSet:CharacterTileSet_Simple_AllCaps forTileFactoryClass:[CharacterTileFactory class]];
//        
//        //Knowledge 0
//        InstructionSection* knowledge0 = [[InstructionSection alloc] initWithInstruction:@"This is your desk."];
//        
//        //Knowledge 1
//        InstructionAndRuleSection* knowledge1 = [[InstructionAndRuleSection alloc] initWithInstruction:@"Here's your first task." andRules:@[Rule_CharB]];
//        //        knowledge1.tile = [[CharacterTile alloc] initWithTileElement:[[Note alloc] initWithMidiValue:60 andBeatCount:1]];
//        knowledge1.numberOfTiles = 1;
//        
//        //Knowledge 2
//        InstructionAndRuleSection* knowledge2 = [[InstructionAndRuleSection alloc] initWithInstruction:@"Good job! Let's try something harder." andRules:@[Rule_CharC, Rule_CharB]];
//        //        knowledge2.tile = [[MusicTile alloc] initWithTileElement:[[Note alloc] initWithMidiValue:62 andBeatCount:1]];
//        knowledge2.numberOfTiles = 2;
//        
//        //Knowledge 3
//        InstructionAndRuleSection* knowledge3 = [[InstructionAndRuleSection alloc] initWithInstruction:@"They did say you were top of your class." andRule:Rule_LetterDescending];
//        knowledge3.numberOfTiles = 3;
//        
//        //Knowledge End
//        InstructionSection* knowledgeEnd = [[InstructionSection alloc] initWithInstruction:@"Let's see if you can handle a real project."];
//        
//        //Initialize rules array
//        NSMutableArray* rulesArray = [NSMutableArray arrayWithObject:Rule_LetterDescending];
//        
//        //Exercise 1
//        InstructionAndRuleSection* exercise1 = [[InstructionAndRuleSection alloc] initWithInstruction:@"Here's your first project." andRules:rulesArray];
//        exercise1.numberOfTiles = 4;

        
        
        //Exercise 1
        //Initialize rules array
        NSMutableArray* rulesArray = [NSMutableArray arrayWithObject:Rule_Fruits];

        //Exercise 1
        RulesSection* exercise1 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise1.numberOfTiles = 4;
        
        //Exercise 2
        [rulesArray insertObject:Rule_CitrusFruit atIndex:0];
        RulesSection* exercise2 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise2.numberOfTiles = 9;

        //Exercise 3
        [rulesArray insertObject:Rule_RedFruits atIndex:0];
        RulesSection* exercise3 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise3.numberOfTiles = 16;
        
        //Exercise 4
        [rulesArray insertObject:Rule_Apple atIndex:0];
        RulesSection* exercise4 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise4.numberOfTiles = 16;
        
        //Exercise 5
        [rulesArray insertObject:Rule_Veggies atIndex:0];
        RulesSection* exercise5 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise5.numberOfTiles = 25;
        
        //Exercise 6
        [rulesArray insertObject:Rule_Carrot atIndex:0];
        RulesSection* exercise6 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise6.numberOfTiles = 25;
        
        //Exercise 7
        [rulesArray insertObject:Rule_GreenVeggies atIndex:0];
        RulesSection* exercise7 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise7.numberOfTiles = 25;
        
        //Exercise 8
        [rulesArray insertObject:Rule_Banana atIndex:0];
        RulesSection* exercise8 = [[RulesSection alloc] initWithRules:rulesArray];
        exercise8.numberOfTiles = 25;
        
        //        knowledge7.numberOfTiles = 9;
        NSMutableArray* sectionsMutable = [NSMutableArray array];
        [sectionsMutable addObject:exercise1];
        [sectionsMutable addObject:exercise2];
        [sectionsMutable addObject:exercise3];
        [sectionsMutable addObject:exercise4];
        [sectionsMutable addObject:exercise5];
        [sectionsMutable addObject:exercise6];
        [sectionsMutable addObject:exercise7];
        [sectionsMutable addObject:exercise8];
//        [sectionsMutable addObject:knowledge0];
//        [sectionsMutable addObject:knowledge1];
//        [sectionsMutable addObject:knowledge2];
//        [sectionsMutable addObject:knowledge3];
//        [sectionsMutable addObject:knowledgeEnd];
//        
//        [sectionsMutable addObject:switcher0];
//        [sectionsMutable addObject:exercise1];
//        [sectionsMutable addObject:exercise2];
//        [sectionsMutable addObject:switcher1];
//        [sectionsMutable addObject:exercise3];
//        [sectionsMutable addObject:exercise4];
//        [sectionsMutable addObject:exercise5];
//        [sectionsMutable addObject:switcher2];
//        [sectionsMutable addObject:exercise6];
//        [sectionsMutable addObject:exercise7];
//        [sectionsMutable addObject:exercise8];
//        [sectionsMutable addObject:exercise9];
//        [sectionsMutable addObject:exercise10];
//        
//        [sectionsMutable addObject:switcher3];
//        [sectionsMutable addObject:exercise11];
//        [sectionsMutable addObject:exercise12];
//        [sectionsMutable addObject:switcher4];
//        [sectionsMutable addObject:exercise13];
//        [sectionsMutable addObject:exercise14];
//        [sectionsMutable addObject:exercise15];
//        [sectionsMutable addObject:exercise16];
//        [sectionsMutable addObject:switcher5];
//        [sectionsMutable addObject:exercise17];
//        [sectionsMutable addObject:exercise18];
//        [sectionsMutable addObject:exercise19];
//        [sectionsMutable addObject:exercise20];
//        
//        
//        [sectionsMutable addObject:switcher6];
//        [sectionsMutable addObject:exercise21];
//        [sectionsMutable addObject:switcher7];
//        [sectionsMutable addObject:exercise22];
//        [sectionsMutable addObject:exercise23];
//        [sectionsMutable addObject:exercise24];
//        [sectionsMutable addObject:exercise25];
        
        [super setSections:[NSArray arrayWithArray:sectionsMutable]];
    }
    
    return [super sections];
}

@end
