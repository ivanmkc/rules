//
//  FoodElement.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileElement.h"

@interface FoodElement : TileElement
@property (strong, nonatomic) NSString* food;
//-(id)initWithFood:(NSString*) food;

typedef enum FoodType
{
    FoodType_VEGETABLE,
    FoodType_SUSHI,
    FoodType_FRUIT,
    FoodType_MEAT,
    FoodType_GRAIN,
    FoodType_SWEETS,
    FoodType_OTHER
} FoodType;

-(id)initWithFood:(NSString*) food ofType:(FoodType) type withAttributes:(NSArray*) attributes;

//-(BOOL) isANumber;
//-(BOOL) isALetter;
@property FoodType type;
@property NSArray* attributes;

@end
