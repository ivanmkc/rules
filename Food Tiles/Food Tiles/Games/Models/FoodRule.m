//
//  CharacterRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FoodRule.h"


@implementation FoodRule
-(BOOL) validatesTile:(Tile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    FoodTile * foodTile= (FoodTile*) tile;
    return [self validatesFoodTile:foodTile andUnpressedTiles:unpressedTiles];
}

-(BOOL) validatesFoodTile:(FoodTile*) foodTile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    NSLog(@"Warning: This function must be implemented by subclasses");
    return YES;
}

-(UIImage*) image
{
    UIImage* image;
    if (_image != nil)
    {
        image = _image;
    }
    else if (self.characterName != nil)
    {
        NSString* imageString = [NSString stringWithFormat:@"rule_%@", [self.characterName lowercaseString]];
        image = [UIImage imageNamed:imageString];
    }
    
    return image;
}

@end
