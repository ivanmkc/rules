//
//  FoodTile.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Tile.h"
#import "FoodElement.h"
@interface FoodTile : Tile
-(FoodElement*) foodElement;
@end
