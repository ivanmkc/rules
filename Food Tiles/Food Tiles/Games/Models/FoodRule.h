//
//  CharacterRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Rule.h"
#import "FoodTile.h"

@interface FoodRule : Rule
@property (strong, nonatomic) UIImage* image;
@property (strong, nonatomic) NSString* characterName;

-(BOOL) validatesFoodTile:(FoodTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
@end
