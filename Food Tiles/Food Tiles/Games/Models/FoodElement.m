//
//  CharacterElement.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FoodElement.h"

@implementation FoodElement
-(id)initWithFood:(NSString*) food ofType:(FoodType) type withAttributes:(NSArray*) attributes;
{
//    UIFont* defaultTypeFace = [UIFont systemFontOfSize:12];
//    return [self initWithCharacter:character andTypeFace:defaultTypeFace];
    self = [super self];
    
    if (self)
    {
        self.food = food;
        self.type = type;
        self.attributes = attributes;
    }
    
    return self;
}
//-(id)initWithCharacter:(NSString*) food andTypeFace:(UIFont*) typeFace
//{
//    self = [super self];
//    
//    if (self)
//    {
//        self.food = food;
//        self.font = typeFace;
//    }
//    
//    return self;
//}

-(NSString*) information
{
    return self.food;
}

//-(BOOL) isALetter
//{
//    NSArray* letters = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z"];
//    return [letters containsObject:self.character];
//}
//
//-(BOOL) isANumber
//{
//    NSArray* numbers = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
//    return [numbers containsObject:self.character];
//}

-(FoodType) getType
{
    return FoodType_FRUIT;
//    if ([self isANumber])
//    {
//        return CharacterType_Number;
//    }
//    else if ([self isALetter])
//    {
//        return CharacterType_Letter;
//    }
//    else
//    {
//        return CharacterType_Symbol;
//    }
}

-(BOOL) isEqual:(FoodElement*)object
{
    if (![super isEqual:object])
    {
        return NO;
    }
    
    if (![self.food isEqualToString:object.food])
    {
        return NO;
    }
    
    if (![self.attributes isEqual:object.attributes])
    {
        return NO;
    }
    
    if (self.type != object.type)
    {
        return NO;
    }
    
    return YES;
}
@end
