//
//  CharacterTileCell.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-22.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FoodTileCell.h"
#import "FoodElement.h"

@interface FoodTileCell()
@end

@implementation FoodTileCell
-(void) setTile:(Tile *) tile
{
    [super setTile:tile];
    
    if ([tile.tileElement isKindOfClass:[FoodElement class]])
    {
        FoodElement* foodElement = (FoodElement*) tile.tileElement;
        self.uiLabelName.text = tile.tileElement.information;
        NSString* imageString = [NSString stringWithFormat:@"food_%@", [foodElement.food lowercaseString]];
        UIImage* fruitImage = [UIImage imageNamed:imageString];
        
        if (fruitImage != nil)
        {
            self.uiImageViewImage.image = fruitImage;
            self.uiImageViewImage.hidden = NO;
            self.uiLabelName.hidden = YES;
        }
        else
        {
            self.uiImageViewImage.hidden = YES;
            self.uiLabelName.hidden = NO;
        }

//        self.uiLabelName.font = characterElement.font;
//        NSLog(characterElement.font.familyName);
    }
}

-(void) setTextColor:(UIColor*) color
{
    self.uiLabelName.textColor = color;
}
@end
