//
//  FoodTileFactory.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileFactory.h"

@interface FoodTileFactory : TileFactory
typedef enum FoodTileSet{
    FoodTileSet_FOOD_SET1_A,
    FoodTileSet_FOOD_SET1_B,
    FoodTileSet_FOOD_SET1_C,
    FoodTileSet_FOOD_SET2_A,
    FoodTileSet_FOOD_SET2_B,
    FoodTileSet_FOOD_SET2_C,
    FoodTileSet_FOOD_SET3_A,
    FoodTileSet_FOOD_SET3_B,
    FoodTileSet_FOOD_SET3_C
} FoodTileSet;

@end
