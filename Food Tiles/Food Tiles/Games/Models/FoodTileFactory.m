
//
//  FoodTileFactory.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FoodTileFactory.h"
#import "FoodElement.h"
#import "FoodTile.h"

@implementation FoodTileFactory
-(id) init
{
    self = [super init];
    
    if (self)
    {
        //Default to simple, all caps
        self.tileSet = FoodTileSet_FOOD_SET1_A;
    }
    
    return self;
}

//Create random music tile
-(Tile*) makeRandomTile
{
//    NSArray* letterSet = [self getLettersForTileSet:self.tileSet];
//    NSArray* fontSet = [self getFontsForTileSet:self.tileSet];
    
    //Roll a die to generate a random tile
//    UInt32 characterStringIndex = arc4random()%(letterSet.count);
//    NSString* characterString = [letterSet objectAtIndex:characterStringIndex];
//    UInt32 fontStringIndex = arc4random()%(fontSet.count);
//    NSString* fontString = [fontSet objectAtIndex:fontStringIndex];
    NSArray* foods = [self getFoods];
    UInt32 foodIndex = arc4random()%(foods.count);
    FoodElement* food = [foods objectAtIndex:foodIndex];

    FoodTile* tile = [[FoodTile alloc] initWithTileElement:food];
    return tile;
}

- (NSArray*) getFoods
{
    NSMutableArray* foods = [NSMutableArray array];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Strawberry" ofType:FoodType_FRUIT withAttributes:@[@"Red"]]];
//    [foods addObject:[[FoodElement alloc] initWithFood:@"Kiwi" ofType:FoodType_FRUIT withAttributes:@[@"Green"]]];
//    [foods addObject:[[FoodElement alloc] initWithFood:@"Cantaloupe" ofType:FoodType_FRUIT withAttributes:@[@"Orange"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Orange" ofType:FoodType_FRUIT withAttributes:@[@"Orange", @"Citrus"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Lemon" ofType:FoodType_FRUIT withAttributes:@[@"Yellow", @"Citrus"]]];
//    [foods addObject:[[FoodElement alloc] initWithFood:@"Lime" ofType:FoodType_FRUIT withAttributes:@[@"Green", @"Citrus"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Banana" ofType:FoodType_FRUIT withAttributes:@[@"Yellow"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Pear" ofType:FoodType_FRUIT withAttributes:@[@"Yellow"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Pomegranate" ofType:FoodType_FRUIT withAttributes:@[@"Red"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Cherry" ofType:FoodType_FRUIT withAttributes:@[@"Red"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Mango" ofType:FoodType_FRUIT withAttributes:@[@"Yellow", @"Orange"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Mango" ofType:FoodType_FRUIT withAttributes:@[@"Yellow", @"Orange"]]];

    [foods addObject:[[FoodElement alloc] initWithFood:@"Apple" ofType:FoodType_FRUIT withAttributes:@[@"Red"]]];
//    [foods addObject:[[FoodElement alloc] initWithFood:@"Avocado" ofType:FoodType_FRUIT withAttributes:@[@"Green"]]];
    
    [foods addObject:[[FoodElement alloc] initWithFood:@"Carrot" ofType:FoodType_VEGETABLE withAttributes:@[@"Orange"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Cucumber" ofType:FoodType_VEGETABLE withAttributes:@[@"Green"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Eggplant" ofType:FoodType_VEGETABLE withAttributes:@[@"Purple"]]];
    
    [foods addObject:[[FoodElement alloc] initWithFood:@"Egg" ofType:FoodType_OTHER withAttributes:@[@"Yellow"]]];


    [foods addObject:[[FoodElement alloc] initWithFood:@"Bacon" ofType:FoodType_MEAT withAttributes:@[@"Green"]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Drumstick" ofType:FoodType_MEAT withAttributes:@[@""]]];
    
    [foods addObject:[[FoodElement alloc] initWithFood:@"Bread" ofType:FoodType_GRAIN withAttributes:@[@""]]];
    [foods addObject:[[FoodElement alloc] initWithFood:@"Croissant" ofType:FoodType_GRAIN withAttributes:@[@"Crescent"]]];
    
    [foods addObject:[[FoodElement alloc] initWithFood:@"Shrimp" ofType:FoodType_MEAT withAttributes:@[@"Seafood", @"Orange"]]];
    
    return [NSArray arrayWithArray:foods];
}

- (NSArray*) getLettersForTileSet:(FoodTileSet) tileSet
{
    NSArray* LETTERS_SIMPLE_ALLCAPS = @[@"A", @"B", @"C", @"D", @"E"];
    NSArray* LETTERS_SIMPLE_MIXEDCASE = [LETTERS_SIMPLE_ALLCAPS arrayByAddingObjectsFromArray:@[@"a", @"b", @"c", @"d", @"e"]];
    NSArray* LETTERS_MEDIUM_MIXEDCASE = [LETTERS_SIMPLE_MIXEDCASE arrayByAddingObjectsFromArray:@[@"F", @"G", @"H", @"I", @"f", @"g", @"h", @"i"]];
    NSArray* LETTERS_SET2_A = [LETTERS_MEDIUM_MIXEDCASE arrayByAddingObjectsFromArray:@[@"J", @"K", @"L", @"M", @"j", @"k", @"l", @"m"]];
    NSArray* LETTERS_SET2_B = [LETTERS_SET2_A arrayByAddingObjectsFromArray:@[@"N", @"O", @"P"]];
    NSArray* LETTERS_SET2_C = [LETTERS_SET2_B arrayByAddingObjectsFromArray:@[@"Q", @"R", @"S"]];
    
    NSArray* LETTERS_SET3_A = [NSArray arrayWithArray:LETTERS_SET2_C];
    NSArray* LETTERS_SET3_B = [LETTERS_SET3_A arrayByAddingObjectsFromArray:@[@"1", @"2", @"3", @"4", @"5"]];
    NSArray* LETTERS_SET3_C = [LETTERS_SET3_B arrayByAddingObjectsFromArray:@[@"6", @"7", @"8", @"9", @"10"]];
    
    NSArray* LETTERS_ALL_MIXEDCASE = [LETTERS_SET2_C arrayByAddingObjectsFromArray:@[@"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z"]];
    
    
    NSArray* letterSet;
    switch (self.tileSet)
    {
        case (FoodTileSet_FOOD_SET1_A):
        {
            letterSet = LETTERS_SIMPLE_ALLCAPS;
            break;
        }
        case (FoodTileSet_FOOD_SET1_B):
        {
            letterSet = LETTERS_SIMPLE_MIXEDCASE;
            break;
        }
        case (FoodTileSet_FOOD_SET1_C):
        {
            letterSet = LETTERS_MEDIUM_MIXEDCASE;
            break;
        }
        case (FoodTileSet_FOOD_SET2_A):
        {
            letterSet = LETTERS_SET2_A;
            break;
        }
        case (FoodTileSet_FOOD_SET2_B):
        {
            letterSet = LETTERS_SET2_B;
            break;
        }
        case (FoodTileSet_FOOD_SET2_C):
        {
            letterSet = LETTERS_SET2_C;
            break;
        }
        case (FoodTileSet_FOOD_SET3_A):
        {
            letterSet = LETTERS_SET3_A;
            break;
        }
        case (FoodTileSet_FOOD_SET3_B):
        {
            letterSet = LETTERS_SET3_B;
            break;
        }
        case (FoodTileSet_FOOD_SET3_C):
        {
            letterSet = LETTERS_SET3_C;
            break;
        }
    }
    
    return letterSet;
}
@end
