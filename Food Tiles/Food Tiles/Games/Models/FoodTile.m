
//
//  FoodTile.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-18.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "FoodTile.h"

@implementation FoodTile

-(FoodElement*) foodElement
{
    return (FoodElement*) self.tileElement;
}

@end
