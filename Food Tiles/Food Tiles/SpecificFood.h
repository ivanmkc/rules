//
//  SpecificFood.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-09-29.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "FoodRule.h"

@interface SpecificFood : FoodRule
@property NSString* food;

-(id)initWithFood:(NSString*) food andDescription:(NSString*) description;
@end
