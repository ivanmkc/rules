//
//  PowerupTileCell.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-01.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "PowerupTileCell.h"

@interface PowerupTileCell()

@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewImage;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelName;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelDescription;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelUpgradeTitle;

@end


@implementation PowerupTileCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) setPowerup:(Powerup*) powerup
{
    self.uiLabelName.text = powerup.name;
    self.uiLabelDescription.text = powerup.information;
    self.uiImageViewImage.image = powerup.image;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
