//
//  FactoryManager.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-19.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rule.h"
#import "TileFactory.h"

@interface TileFactoryManager : NSObject
+(TileFactory*) getInstance;
+(void) setInstance:(TileFactory*) tileFactory;

+(UINib*) getNibInstance;
+(void) setNibInstance:(UINib*) nib;
@end
