//
//  Note.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicElement.h"

@interface Note : MusicElement
@property int midiValue;
-(id)initWithMidiValue:(int) midiValue andBeatCount:(int) beatCount;
@end
