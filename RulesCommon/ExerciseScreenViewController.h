//
//  ExerciseScreenViewController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RulesSection.h"
#import "UIViewControllerWithDecoratedTileCollectionView.h"

@protocol ExerciseScreenViewControllerDelegate <NSObject>
@optional
- (void)didFinishWithPassState:(BOOL)isPassed isRetryingCurrentExercise:(BOOL)isRetrying;
@end

@interface ExerciseScreenViewController : UIViewControllerWithDecoratedTileCollectionView
@property (weak, nonatomic) RulesSection *instructionAndRuleSection;
@property int projectNumber;

@property(weak, nonatomic) id <ExerciseScreenViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *uiLabelInstructions;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelRule;
@property (weak, nonatomic) IBOutlet TileCollectionView *uiTileCollectionView;

-(void)prepareExerciseTutorial;
-(void)showTutorial;
-(void)restartExercise;
@end
