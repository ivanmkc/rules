//
//  LineOrSpaceNoteRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-14.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "LineOrSpaceNoteRule.h"
#import "Note.h"

@implementation LineOrSpaceNoteRule
-(id)initWithLinesMode:(BOOL) isLinesMode andDescription:(NSString*) description;
{
    self = [super init];
    
    if (self)
    {
        self.isLinesMode = isLinesMode;
        self.information = description;
    }
    
    return self;
}

-(BOOL) validatesMusicTile:(MusicTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[Note class]])
    {
        Note* note = (Note*) tile.tileElement;
        //Determine if the note is on a line or not
        //TODO: this assumes the midi note is given the common name (e.g. B-flat instead of A-sharp)
        NSNumber* remainder = @(note.midiValue % 12);
        //Check if they are valid notes (i.e. not sharps or flats)
        NSArray* validRemainders = @[@0,@2,@4,@5,@7,@9,@11];
        if ([validRemainders containsObject:remainder])
        {
            NSArray* validLineRemainders = @[@0, @4, @7, @11];
            //Get octave
            int octaveNumber = floor(note.midiValue / 12.0f);
            BOOL isOctaveNumberOdd = (octaveNumber % 2)==1;
            
            //See if the note is on a line
            BOOL isLinesNote = ([validLineRemainders containsObject:remainder])&&isOctaveNumberOdd;
            
            //Validate according to what mode is
            if (isLinesNote==self.isLinesMode)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            NSLog(@"Warning: This rule only works with natural notes");
            return NO;
        }
    }
    else
    {
        return NO;
    }
}
@end
