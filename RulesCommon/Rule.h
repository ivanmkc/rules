//
//  Rule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tile.h"

@interface Rule : NSObject
@property (strong, nonatomic) NSString* description;
-(id) initWithDescription:(NSString*) description;
-(BOOL) validatesTile:(Tile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;

@property (strong, nonatomic) NSString* information;
@end
