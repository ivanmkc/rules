//
//  Note.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Note.h"

@implementation Note
-(id)initWithMidiValue:(int) midiValue andBeatCount:(int) beatCount
{
    self = [super self];
    
    if (self)
    {
        self.midiValue = midiValue;
        self.beatCount = beatCount;
    }
    
    return self;
}

-(NSString*) information
{
    return [@(self.midiValue) stringValue];
}

@end
