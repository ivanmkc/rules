//
//  PendingActivityModalView.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingActivityModalView : UIView

+ (PendingActivityModalView *) showInView: (UIView *) parentView withText: (NSString *) text;

- (void) dismiss;

@end
