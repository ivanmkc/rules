//
//  PowerupManager.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-01.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Powerup.h"

@interface PowerupManager : NSObject
+ (PowerupManager*) instance;

-(NSArray*) getPowerups;
-(void) addPowerup:(Powerup*) powerup;
-(void) removePowerup:(Powerup*) powerup;
-(Powerup*) getRandomPowerup;
-(NSArray*) getRandomPowerups:(int) numberOfPowerups;
-(Powerup*) getPowerupWithName:(NSString*) name;
@end
