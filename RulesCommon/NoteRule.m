//
//  NoteRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "NoteRule.h"

@implementation NoteRule
-(id)initWithNote:(Note*) note andDescription:(NSString*) description;
{
    self = [super initWithDescription:(NSString*) description];
    
    if (self)
    {
        self.note = note;
    }
    
    return self;
}

-(BOOL) validatesMusicTile:(MusicTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[Note class]])
    {
        Note* note = (Note*) tile.tileElement;
        return (note.midiValue == self.note.midiValue);
    }
    else
    {
        return NO;
    }
}

@end
