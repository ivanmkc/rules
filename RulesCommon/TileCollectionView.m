//
//  TileCollectionView.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileCollectionView.h"
#import "TileFactory.h"
#import "TileCell.h"

//Music
#import "TileFactoryManager.h"

@interface TileCollectionView ()
@property(weak, nonatomic) id <TileCollectionViewDelegate> secondDelegate;

@property int currentRuleIndex;

@property (strong, nonatomic) NSMutableArray* tiles;
@property (strong, nonatomic) NSMutableArray* unpressedTiles;
@property (strong, nonatomic) NSMutableArray* pressedTilesIndexPaths;

@property (strong, readonly) TileFactory* tileFactory;
@end

static const int kNumberOfTiles = 16;

@implementation TileCollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

//Set self as delegate and datasource
- (void)awakeFromNib
{
    self.delegate = self;
    self.dataSource = self;
    self.numberOfTiles = kNumberOfTiles;
}

-(TileFactory*) tileFactory
{
    return [TileFactoryManager getInstance];
}

-(Rule*) getCurrentRule
{
    //Get new rule
    Rule* currentRule = nil;
    if (self.currentRuleIndex < self.rules.count)
    {
        currentRule = [self.rules objectAtIndex:self.currentRuleIndex];
    }
    
    return currentRule;
}

-(void)setNumberOfTiles:(int)numberOfTiles
{
    _numberOfTiles = numberOfTiles;
    
    [self reloadTiles];
}

-(void)setRules:(NSArray *)rules
{
    _rules = rules;
    
    [self reloadTiles];
}

-(void)reloadTiles
{
    //Load tiles
    self.tiles = [NSMutableArray arrayWithArray:[self.tileFactory makeTiles:self.numberOfTiles withRules:self.rules]];
    self.unpressedTiles = [NSMutableArray arrayWithArray:self.tiles];
    self.pressedTilesIndexPaths = [NSMutableArray array];
    
    //Set first rule
    self.currentRuleIndex = 0;
    Rule* currentRule = [self getCurrentRule];
    if (currentRule)
    {
        //Fire the rule changed delegate
        if ([self.secondDelegate respondsToSelector:@selector(onRuleReplaced:byNewRule:)])
        {
            [self.secondDelegate onRuleReplaced:nil byNewRule:currentRule];
        }
    }
    
    //Fire the tiles reloaded delegate
    if ([self.secondDelegate respondsToSelector:@selector(onTilesReloaded)])
    {
        [self.secondDelegate onTilesReloaded];
    }
    
    //Reload the view
//    [self reloadData];
//    dispatch_async(dispatch_get_main_queue(), ^{
    
//    NSMutableArray* allIndexPaths = [NSMutableArray array];
//    for (int sectionNumber = 0; sectionNumber<self.numberOfSections; sectionNumber++)
//    {
//        for (int itemNumber = 0; itemNumber<[self numberOfItemsInSection:sectionNumber]; itemNumber++)
//        {
//            [allIndexPaths addObject:[NSIndexPath indexPathForItem:itemNumber inSection:sectionNumber]];
//        }
//    }
//    [self reloadItemsAtIndexPaths:allIndexPaths];
    [self reloadData];
//        [self.collectionViewLayout invalidateLayout];
//    });
}

#pragma mark - Game logic

//Check if the current rule is fulfilled
-(BOOL)checkIsRuleComplete
{
    assert(self.currentRuleIndex < self.rules.count);
    
    BOOL isComplete = true;
    for (Tile* tile in self.unpressedTiles)
    {
        Rule* currentRule = [self.rules objectAtIndex:self.currentRuleIndex];
        if ([currentRule validatesTile:tile andUnpressedTiles:self.unpressedTiles])
        {
            isComplete = false;
            break;
        }
    }
    
    return isComplete;
}

//Check if this tutorial has met the finish condition
-(BOOL)checkIsFinished
{
    int numberOfCorrectTiles = self.pressedTilesIndexPaths.count;
    return (numberOfCorrectTiles >= self.numberOfTiles)||(self.currentRuleIndex >= self.rules.count);
}

-(void)finishTutorial
{
    if ([self.secondDelegate respondsToSelector:@selector(onAllRulesCompleted)]) {
        [self.secondDelegate onAllRulesCompleted];
    }
//    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CollectionView delegate and datasource
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Tile* tile = [self.tiles objectAtIndex:indexPath.item];
    
    //Get cell for tile
    TileCell* tileCell = (TileCell*) [collectionView cellForItemAtIndexPath:indexPath];
    
    Rule* currentRule = [self.rules objectAtIndex:self.currentRuleIndex];
    BOOL isTileCleared = NO;
    if (([currentRule validatesTile:tile andUnpressedTiles:self.unpressedTiles])&&(![self.pressedTilesIndexPaths containsObject:indexPath]))
    {
        //Set cell to pressed state
        tileCell.isPressed = YES;
        
        //Set cell touch interaction to off
        tileCell.userInteractionEnabled = NO;
        
        //Add to pressed index paths
        [self.pressedTilesIndexPaths addObject:indexPath];
        
        //Remove from unpressed tiles array
        //TODO: check if this will remove all instances of one note
        [self.unpressedTiles removeObject:tile];
        
        isTileCleared = YES;
        
        NSLog(@"%i tiles left!", self.unpressedTiles.count);
        
        //TODO: add while loop to check all rules until one is not complete
        //Check if the rule is complete
        while ([self checkIsRuleComplete])
        {
            NSLog(@"Rule Completed: %@", currentRule.information);
            
            self.currentRuleIndex++;
            
            if (self.currentRuleIndex < self.rules.count)
            {
                //Get new rule
                Rule* newRule = [self getCurrentRule];
                
                if ([self.secondDelegate respondsToSelector:@selector(onRuleReplaced:byNewRule:)])
                {
                    [self.secondDelegate onRuleReplaced:currentRule byNewRule:newRule];
                }
                
                currentRule = newRule;
            }
            else
            {
                break;
            }
        }
        
        //Check if finished
        if ([self checkIsFinished])
        {
            NSAssert(self.unpressedTiles.count == 0, @"Exercise completed even with remaining tiles!");
            
            NSLog(@"Tiles Completed!");
            [self finishTutorial];
        }
    }
    
    //Fire delegate method
    if ([self.secondDelegate respondsToSelector:@selector(TileCollectionView:didSelectItemAtIndexPath:)])
    {
        [self.secondDelegate TileCollectionView:self didSelectItemAtIndexPath:indexPath];
    }
    
    if ([self.secondDelegate respondsToSelector:@selector(onTileTapped:wasCleared:)])
    {
        [self.secondDelegate onTileTapped:tileCell wasCleared:isTileCleared];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.tiles.count;
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
//{
//    return 4;
//}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellIdentifier = @"TileCell";
    TileCell *cell = nil;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.autoresizingMask = UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin;
    
    Tile* tile = [self.tiles objectAtIndex:indexPath.item];
    [cell setTile:tile];
    
    //Unhide and unlock cell if it was hidden before
    cell.hidden = NO;
    cell.userInteractionEnabled = YES;
    
    BOOL isPressed = [self.pressedTilesIndexPaths containsObject:indexPath];
    [cell setIsPressed:isPressed];
    
    //Fire delegate function
    if ([self.secondDelegate respondsToSelector:@selector(onTileCreated:forTile:)])
    {
        [self.secondDelegate onTileCreated:cell forTile:tile];
    }
    
    return cell;
}

//Override set delegate so it sets itself as the delegate but saves another delegate to pass events on to
-(void)setDelegate:(id) targetDelegate
{
    //    _delegate = self;
    if (targetDelegate != self)
    {
        self.secondDelegate = targetDelegate;
    }
    else
    {
        [super setDelegate:targetDelegate];
    }
}

//Override set delegate so it sets itself as the delegate but saves another delegate to pass events on to
-(void)setDataSource:(id<UICollectionViewDataSource>)dataSource
{
    if (dataSource != self)
    {
        NSLog(@"Warning: Datasource cannot be set. Must be self");
    }
    else
    {
        [super setDataSource:dataSource];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark - FlowLayout delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int tilesPerSide = round(sqrt(self.numberOfTiles));
    
    if (tilesPerSide*tilesPerSide != self.numberOfTiles)
    {
        //        float widthPerSide = self.uiTileCollectionView.frame.size.width/tilesPerSide;
        tilesPerSide = self.numberOfTiles;
    }
    
    float widthPerSide = fmin(collectionView.frame.size.width/tilesPerSide, collectionView.frame.size.height/tilesPerSide);
    
    return CGSizeMake(widthPerSide, widthPerSide);
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, margin, 0, margin);
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

@end
