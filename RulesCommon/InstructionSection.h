//
//  InstructionAndContinueSection.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-24.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstructionSection : NSObject
@property (strong, nonatomic) NSString* instruction;

-(id)initWithInstruction:(NSString*) instruction;
@end
