//
//  PowerupScreenViewController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-01.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "PowerupScreenViewController.h"
#import "PowerupManager.h"
#import "Powerup.h"
#import "ProgressionManager.h"

#import "PowerupTileCell.h"

@interface PowerupScreenViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *uiCollectionViewPowerups;
@property (weak, nonatomic) IBOutlet UIButton *uiButtonPurchase;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelScore;

@property (strong, nonatomic) NSArray* powerups;
@end

@implementation PowerupScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.uiLabelScore.text = [@([[ProgressionManager instance] getNumberOfCoins]) stringValue];
    Powerup* rubyPowerup = [[PowerupManager instance] getPowerupWithName:@"Ruby"];
    self.powerups = [[[PowerupManager instance] getRandomPowerups:2] arrayByAddingObject:rubyPowerup];
}

-(void) viewWillAppear:(BOOL)animated
{
    //Register nibs
    [self.uiCollectionViewPowerups registerNib:[UINib nibWithNibName:@"PowerupTileCell" bundle:[NSBundle mainBundle]]
                forCellWithReuseIdentifier:@"PowerupCell"];
}

-(void) viewDidAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) purchasePowerup:(Powerup*) powerup
{
    //If have enough points
    if ([powerup.name isEqualToString:@"Ruby"])
    {
        [[ProgressionManager instance] addCoins:1];
        
        //Disable user interaction so user can't tap multiple times
        self.uiCollectionViewPowerups.userInteractionEnabled = NO;
        
        //Pop to root view
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([[ProgressionManager instance] getNumberOfCoins] >= powerup.cost)
    {
        [[ProgressionManager instance] subtractCoins:powerup.cost];
        [[ProgressionManager instance] addPurchasedProduct:powerup.name];
        
        //Disable user interaction so user can't tap multiple times
        self.uiCollectionViewPowerups.userInteractionEnabled = NO;
        
        //Update total points
//        
//        //Update UI
//        [self.uiCollectionViewPowerups reloadData];
//        powerup = [self getCurrentPowerup];
//        [self updatePurchaseButtonForPowerup:powerup];
        
        self.uiLabelScore.text = [@([[ProgressionManager instance] getNumberOfCoins]) stringValue];
        
        //Pop to root view
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        //Display not enough points message and get them to buy some
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Collection view delegate functions
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.powerups.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellIdentifier = @"PowerupCell";
    PowerupTileCell *cell = nil;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Powerup* powerup = [self.powerups objectAtIndex:indexPath.item];
    
    [cell setPowerup:powerup];
    
//    BOOL isPressed = [self.pressedTilesIndexPaths containsObject:indexPath];
//    [cell setIsPressed:isPressed];
    
    //Fire delegate function
//    if ([self.secondDelegate respondsToSelector:@selector(onTileCreated:forTile:)])
//    {
//        [self.secondDelegate onTileCreated:cell forTile:tile];
//    }
    
    return cell;
}

//Purchase powerup
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Powerup* powerup = [self.powerups objectAtIndex:indexPath.item];
    [self purchasePowerup:powerup];
}

@end
