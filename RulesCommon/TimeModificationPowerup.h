//
//  TimeModificationPowerup.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Powerup.h"

@interface TimeModificationPowerup : Powerup
@property int secondsToIncrease;

-(id) initWithName:(NSString*) name andDescription:(NSString*)description andEffectDescription:(NSString*) effectDescription andCost:(int) cost andTimeIncrease:(int) secondsToIncrease;
-(id) activatePowerupOnParameter:(NSObject*) parameter;
@end
