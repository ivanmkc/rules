//
//  MusicElement.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TileElement.h"

@interface MusicElement : TileElement
@property int beatCount;
@end
