//
//  PendingActivityModalView.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "PendingActivityModalView.h"

@interface PendingActivityModalView ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation PendingActivityModalView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (PendingActivityModalView *) showInView: (UIView *) parentView withText: (NSString *) text;
{
    PendingActivityModalView * view = [[[NSBundle mainBundle] loadNibNamed: @"PendingActivityModalView" owner:self options:nil ]firstObject ];

    view.textLabel.text = text;
    view.frame = parentView.frame;

    [parentView addSubview: view];
    
    return view;
}

- (void) dismiss
{
    [self removeFromSuperview];
}

@end
