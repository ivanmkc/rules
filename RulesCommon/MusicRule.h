//
//  MusicRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Rule.h"
#import "MusicTile.h"

@interface MusicRule : Rule
-(BOOL) validatesMusicTile:(MusicTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
@end
