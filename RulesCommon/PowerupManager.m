//
//  PowerupManager.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-01.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "PowerupManager.h"

@interface PowerupManager()
@property (strong, nonatomic) NSMutableArray* powerups;
@end

@implementation PowerupManager

static PowerupManager *sInstance;

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sInstance = [[PowerupManager alloc] init];
    }
}

+ (PowerupManager*)instance
{
    NSAssert(sInstance != nil, @"not initialized!");
    return sInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.powerups = [NSMutableArray array];
    }
    
    return self;
}

//Powerup types
-(NSArray*) getPowerups
{
    return [NSArray arrayWithArray:self.powerups];
}

-(void) addPowerup:(Powerup*) powerup
{
    [self.powerups addObject:powerup];
}

-(void) removePowerup:(Powerup*) powerup
{
    [self.powerups removeObject:powerup];
}

-(Powerup*) getRandomPowerup
{
    return [[self getRandomPowerups:1] firstObject];
}

-(NSArray*) getRandomPowerups:(int) numberOfPowerups
{
    NSMutableArray* availablePowerups = [NSMutableArray arrayWithArray:self.powerups];
    
    //Remove ruby
    Powerup* rubyPowerup = [self getPowerupWithName:@"Ruby"];
    [availablePowerups removeObject:rubyPowerup];
    
    NSMutableArray* randomPowerups = [NSMutableArray array];
    
    //Get random powerups
    for (int powerupNumber = 0; powerupNumber<numberOfPowerups; powerupNumber++)
    {
        UInt32 powerupIndex = arc4random()%(availablePowerups.count);
        [randomPowerups addObject:[availablePowerups objectAtIndex:powerupIndex]];
        
        //Remove from available powerups so we don't choose it again
        [availablePowerups removeObjectAtIndex:powerupIndex];
    }
    
    return [NSArray arrayWithArray:randomPowerups];
}

-(Powerup*) getPowerupWithName:(NSString*) name
{
    return [self.powerups objectAtIndex:[self.powerups indexOfObjectPassingTest:
         ^BOOL(Powerup* obj, NSUInteger idx, BOOL *stop) {
            return [obj.name isEqualToString:name];
         }
    ]];
}
@end
