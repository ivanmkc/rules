//
//  ScoreScreenViewController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreScreenViewController : UIViewController
@property int secondsLeft;
@property int secondsTotal;
@property int projectNumber;
@end
