//
//  TileElement.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileElement.h"

@implementation TileElement

-(NSString*) information
{
    return nil;
}

-(BOOL) isEqual:(id)object
{
    if (![self isKindOfClass:[object class]])
    {
        return NO;
    }
    
    return YES;
}
@end
