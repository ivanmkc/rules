//
//  LineOrSpaceNoteRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-14.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicRule.h"
#import "Note.h"

@interface LineOrSpaceNoteRule : MusicRule
@property BOOL isLinesMode;
-(id)initWithLinesMode:(BOOL) isLinesMode andDescription:(NSString*) description;
@end
