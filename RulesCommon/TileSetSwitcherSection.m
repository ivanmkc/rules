//
//  TileSetSwitch.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-22.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileSetSwitcherSection.h"

@implementation TileSetSwitcherSection
- (id) initWithTargetTileSet:(int) targetTileSet forTileFactoryClass:(Class) tileFactoryClass
{
    self = [super init];
    
    if (self)
    {
        self.tileSet = targetTileSet;
        self.tileFactoryClass = tileFactoryClass;
    }
    
    return self;
}
@end
