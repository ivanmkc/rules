//
//  TileCollectionView.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Rule.h"
#import "TileCell.h"

@class TileCollectionView;
@protocol TileCollectionViewDelegate <NSObject>
@optional
- (void) onTileCreated:(TileCell*)tileCell forTile:(Tile*) tile;
- (void) TileCollectionView:(TileCollectionView *)TileCollectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath;
- (void) onTilesReloaded;
- (void) onRuleReplaced:(Rule*) oldRule byNewRule:(Rule*) newRule;
- (void) onTileTapped:(TileCell*)tileCell wasCleared:(BOOL) wasCleared;
- (void) onAllRulesCompleted;
@end

@interface TileCollectionView : UICollectionView<UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) NSArray* rules;

//Number of tiles. Defaults to 16
@property (nonatomic) int numberOfTiles;
-(Rule*) getCurrentRule;
-(void) reloadTiles;
@end
