//
//  Powerup.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Powerup : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* information;

@property int cost;

@property (strong, nonatomic) UIImage* image;

-(id) initWithName:(NSString*) name andDescription:(NSString*)description andCost:(int) cost;
-(id) activatePowerupOnParameter:(NSObject*) parameter;

@end
