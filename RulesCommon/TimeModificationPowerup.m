//
//  TimeModificationPowerup.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-02.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TimeModificationPowerup.h"

@implementation TimeModificationPowerup

-(id) initWithName:(NSString*) name andDescription:(NSString*)description andEffectDescription:(NSString*) effectDescription andCost:(int) cost andTimeIncrease:(int) secondsToIncrease;
{
    self = [super initWithName:name andDescription:description andCost:cost];
    
    if (self)
    {
        self.secondsToIncrease = secondsToIncrease;
    }
    
    return self;
}

-(id) activatePowerupOnParameter:(NSObject*) parameter
{
    if ([parameter isKindOfClass:[NSNumber class]])
    {
        NSNumber* number = (NSNumber*) parameter;
        return @([number integerValue] + self.secondsToIncrease);
    }
    else
    {
        return nil;
    }
}

@end
