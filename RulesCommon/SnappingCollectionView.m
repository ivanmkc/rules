//
//  SnappingCollectionView.m
//  iOSAudio
//
//  Created by Ivan Cheung on 2014-06-03.
//  Copyright (c) 2014 George Tam. All rights reserved.
//

#import "SnappingCollectionView.h"

@interface SnappingCollectionView ()
@property(weak, nonatomic) id <SnappingCollectionViewDelegate> secondDelegate;
@end

@implementation SnappingCollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}
//
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        [self commonInit];
//    }
//    return self;
//}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Do something
    [self commonInit];
}

- (void)commonInit
{
    // Initialization code
    //Set decceleration speed to fast
    self.delegate = self;
    self.decelerationRate = UIScrollViewDecelerationRateFast;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //Get index path of cell at center
    CGPoint centerPoint = CGPointMake(self.center.x + self.contentOffset.x, 100);
    NSIndexPath *centerCellIndexPath = [self indexPathForItemAtPoint:centerPoint];
    
    //If selected index path is already in the centetr
    if ([indexPath compare:centerCellIndexPath] == NSOrderedSame)
    {
        if ([self.secondDelegate respondsToSelector:@selector(snappingCollectionView:didSelectItemAtIndexPath:)]) {
            [self.secondDelegate snappingCollectionView:self didSelectItemAtIndexPath:centerCellIndexPath];
        }
    }
    else
    {
        //Scroll to the selected cell
        [self selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    //Notify delegate of selection
    if ([self.secondDelegate respondsToSelector:@selector(snappingCollectionView:didSelectItemAtIndexPath:)]) {
        //Get cell at center of scrollview
        CGPoint centerPoint = CGPointMake(self.center.x + self.contentOffset.x, 100);
        NSIndexPath *centerCellIndexPath = [self indexPathForItemAtPoint:centerPoint];
        
        [self.secondDelegate snappingCollectionView:self didSelectItemAtIndexPath:centerCellIndexPath];
    }
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint centerPoint = CGPointMake(self.center.x + self.contentOffset.x, 100);
    NSIndexPath *centerCellIndexPath = [self indexPathForItemAtPoint:centerPoint];

    //Notify delegate of snapping
    if ([self.secondDelegate respondsToSelector:@selector(snappingCollectionView:didSnapToIndexPath:)]) {
        [self.secondDelegate snappingCollectionView:self didSnapToIndexPath:centerCellIndexPath];
    }
    
    [self selectItemAtIndexPath:centerCellIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
}

// Snapping action
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGFloat proposedContentOffsetCenterX = targetContentOffset->x + self.bounds.size.width * 0.5f;
    
    CGRect proposedRect = self.bounds;
    
    // Comment out if you want the collectionview simply stop at the center of an item while scrolling freely
    // proposedRect = CGRectMake(proposedContentOffset.x, 0.0, collectionViewSize.width, collectionViewSize.height);
    
    UICollectionViewLayoutAttributes* candidateAttributes;
    for (UICollectionViewLayoutAttributes* attributes in [self.collectionViewLayout layoutAttributesForElementsInRect:proposedRect])
    {
        
        // == Skip comparison with non-cell items (headers and footers) == //
        if (attributes.representedElementCategory != UICollectionElementCategoryCell)
        {
            continue;
        }
        
        // == First time in the loop == //
        if(!candidateAttributes)
        {
            candidateAttributes = attributes;
            continue;
        }
        
        if (fabsf(attributes.center.x - proposedContentOffsetCenterX) < fabsf(candidateAttributes.center.x - proposedContentOffsetCenterX))
        {
            candidateAttributes = attributes;
        }
    }
    
    *targetContentOffset = CGPointMake(candidateAttributes.center.x - self.bounds.size.width * 0.5f, targetContentOffset->y);
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 400;
//}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    float cellWidth = [self cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]].frame.size.width;
//    return UIEdgeInsetsMake(0, (1024-cellWidth)/2, 0, (1024-cellWidth)/2);
//}

//Override set delegate so it sets itself as the delegate but saves another delegate to pass events on to
-(void)setDelegate:(id) targetDelegate
{
//    _delegate = self;
    if (targetDelegate != self)
    {
        self.secondDelegate = targetDelegate;
    }
    else
    {
        [super setDelegate:targetDelegate];
    }
}

-(void)zoomCell:(UICollectionViewCell*) cell
{
    CALayer *shapeLayer = cell.layer;
    shapeLayer.anchorPoint = CGPointMake(.5,.5);
    shapeLayer.contentsGravity = @"center";
    
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue =   [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)];
    
    [animation setDuration:0.2];
    animation.fillMode=kCAFillModeForwards;
    animation.removedOnCompletion=NO;
    [shapeLayer addAnimation:animation forKey:@"zoom"];
}
@end
