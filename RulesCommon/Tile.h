//
//  Tile.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TileElement.h"

@interface Tile : NSObject
@property (strong, nonatomic) TileElement* tileElement;
@property (strong, nonatomic) UIImage* image;
-(id) initWithTileElement:(TileElement *) element;
@end
