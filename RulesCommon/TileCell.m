    //
//  MusicTileCellController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileCell.h"
#import "Tile.h"
#import "Note.h"

@interface TileCell()
@property (weak, nonatomic) IBOutlet UIImageView *uiImageHint;
@end

@implementation TileCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isShowingHints = NO;
        self.isValidatedByRule = NO;
    }
    return self;
}

-(void) setTile:(Tile *) tile
{
    _tile = tile;
    
    if (tile.image)
    {
        self.uiImageViewImage.image = tile.image;
    }
    else
    {
        self.uiLabelName.text = tile.tileElement.information;
    }
}

-(void) setIsPressed:(BOOL)isPressed
{
    _isPressed = isPressed;
    
//    self.hidden = isPressed;
}

-(void) setIsShowingHints:(BOOL)isShowingHints
{
    _isShowingHints = isShowingHints;
    
    //Hide\show hint
    self.uiImageHint.hidden = !isShowingHints;
    
    //Change background of tile
//    self.uiImageViewBackground.image = !isShowingHints ? [UIImage imageNamed:@"Tile_Regular"] : [UIImage imageNamed:@"Tile_Tutorial"];
}

-(void) setIsValidatedByRule:(BOOL)isValidatedByRule
{
    _isValidatedByRule = isValidatedByRule;
    
    //Set the image for the hint
    self.uiImageHint.image = isValidatedByRule ? [UIImage imageNamed:@"Check"] : [UIImage imageNamed:@"Cross"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
