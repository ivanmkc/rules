//
//  ProgressionManager.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProgressionManager : NSObject
+ (ProgressionManager*) instance;

//Product purchases
- (BOOL) isProductPurchased:(NSString *)productIdentifier;
- (int) getPurchasedProductCount:(NSString *)productIdentifier;
- (void) addPurchasedProduct:(NSString *)productIdentifier;
- (void) removePurchasedProduct:(NSString *)productIdentifier;
- (NSArray*) getPurchasedProductIds;

//Coins
-(void) addCoins:(int) score;
-(void) subtractCoins:(int) score;
-(int) getNumberOfCoins;

//Levels
@property (nonatomic) int highestLevelReached;

//Score
@property (nonatomic) int highestScoreReached;

//Temporary data
@property int currentScore;
@end
