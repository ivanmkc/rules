//
//  Powerup.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Powerup.h"

@implementation Powerup

-(id) initWithName:(NSString*) name andDescription:(NSString*)description andCost:(int) cost
{
    self = [super init];
    if (self)
    {
        self.name = name;
        self.information = description;
        self.cost = cost;
    }
    return self;
}

-(UIImage*) image
{
    if (_image == nil)
    {
        _image = [UIImage imageNamed:[[NSString stringWithFormat:@"powerup_%@", self.name] lowercaseString]];
    }
    return _image;
}

-(id) activatePowerupOnParameter:(NSObject*) parameter
{
    return nil;
}

@end
