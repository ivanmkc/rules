
//
//  ProgressionManager.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "ProgressionManager.h"

@interface ProgressionManager ()
//@property (weak, nonatomic) PFUser* currentUser;

@property int totalCoins;

//Dictionary of lessons (key: lesson id) and (value: final score)
@property (strong, nonatomic) NSMutableSet* purchasedProductIdentifiers;

@end

@implementation ProgressionManager

static ProgressionManager *sInstance;

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sInstance = [[ProgressionManager alloc] init];
    }
}

+ (ProgressionManager*)instance
{
    NSAssert(sInstance != nil, @"not initialized!");
    return sInstance;
}

- (id)init
{
    self = [super init];
    if( !self )
    {
        return nil;
    }
    
    //Defaults
    _purchasedProductIdentifiers = [[NSMutableSet alloc] init];
    _highestLevelReached = 0;
    _highestScoreReached = 0;
    _currentScore = 0;
    _totalCoins = 0;
    
    // load existing progress that's been saved
    [self load];
    
    if (self.purchasedProductIdentifiers == nil)
    {
        self.purchasedProductIdentifiers = [NSMutableSet set];
    }
    
    return self;
}

#pragma mark - Score
-(void) addCoins:(int) score
{
    self.totalCoins += score;
    [self save];
}

-(void) subtractCoins:(int) score
{
    self.totalCoins -= score;
    if (self.totalCoins < 0)
    {
        self.totalCoins = 0;
    }
    [self save];
}

-(int) getNumberOfCoins
{
    return self.totalCoins;
}

#pragma mark - Purchased products
- (BOOL) isProductPurchased:(NSString *)productIdentifier
{
//#if DEBUG
//    return YES;
//#endif
    
    return [self.purchasedProductIdentifiers containsObject:productIdentifier];
}

- (int) getPurchasedProductCount:(NSString *)productIdentifier
{
    int count = 0;
    for (NSString* productId in self.purchasedProductIdentifiers)
    {
        if ([productId isEqualToString:productIdentifier])
        {
            count++;
        }
    }
    return count;
}

- (void) addPurchasedProduct:(NSString *)productIdentifier
{
    [self.purchasedProductIdentifiers addObject:productIdentifier];
    [self save];
}

- (void) removePurchasedProduct:(NSString *)productIdentifier
{
    [self.purchasedProductIdentifiers removeObject:productIdentifier];
    [self save];
}

- (NSArray*) getPurchasedProductIds
{
    return [self.purchasedProductIdentifiers allObjects];
}

#pragma mark - Levels
- (void) setHighestLevelReached:(int)highestLevelReached
{
    _highestLevelReached = highestLevelReached;
    [self save];
}

#pragma mark - Data Persistence
- (void)load
{
    self.totalCoins = [[NSUserDefaults standardUserDefaults] integerForKey:@"TotalScore"];
    self.purchasedProductIdentifiers = [NSMutableSet setWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"PurchasedProductIdentifiers"]];
    self.highestLevelReached = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighestLevelReached"];
}

- (void)save
{
    [[NSUserDefaults standardUserDefaults] setInteger:self.totalCoins forKey:@"TotalScore"];
    //Save purchased product identifiers as NSArray, as NSSet is not allowed
    [[NSUserDefaults standardUserDefaults]  setObject:[self.purchasedProductIdentifiers allObjects] forKey:@"PurchasedProductIdentifiers"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.highestLevelReached forKey:@"HighestLevelReached"];
    
    //Force the save to disk
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end