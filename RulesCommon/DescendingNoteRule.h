//
//  DescendingNoteRule.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicRule.h"
#import "MusicTile.h"

@interface DescendingNoteRule : MusicRule
@end
