//
//  ScoreScreenViewController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-31.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "ScoreScreenViewController.h"
#import "ProgressionManager.h"

@interface ScoreScreenViewController ()
@property (weak, nonatomic) IBOutlet UILabel *uiLableTitle;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelScore;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelTotalScore;

@property int score;
@property (strong, nonatomic) NSString* projectTitle;

@property int currentIncrementingScore;
@property (strong, nonatomic) NSTimer* scoreIncrementingTimer;
@end

@implementation ScoreScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.score = self.secondsLeft * self.projectNumber;
    
    //Calculate maximum possible score
    int maxScore = self.secondsTotal*self.projectNumber;
    self.uiLabelScore.text = [NSString stringWithFormat:@"%i out of %i pts", self.score, maxScore];
    self.uiLableTitle.text = self.projectTitle;
    
    //Show score before adding new score
    self.currentIncrementingScore = [ProgressionManager instance].currentScore;
    self.uiLabelTotalScore.text = [@(self.currentIncrementingScore) stringValue];
    
    //Save score
    [ProgressionManager instance].currentScore += self.score;
    
    //Check if score record is broken
    if ([ProgressionManager instance].highestScoreReached < [ProgressionManager instance].currentScore)
    {
        [ProgressionManager instance].highestScoreReached = [ProgressionManager instance].currentScore;
    }
    
    //Show total score
//    self.uiLabelTotalScore.text = [@([[ProgressionManager instance] getScore]) stringValue];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self startScoreIncrementingAnimation];
}

- (void)startScoreIncrementingAnimation
{
    [self.scoreIncrementingTimer invalidate];        // stop timer if one is running already
    self.scoreIncrementingTimer = [NSTimer scheduledTimerWithTimeInterval:0.03f target:self selector:@selector(scoreIncrementingTimerFired:) userInfo:nil repeats:YES];
}

- (void)scoreIncrementingTimerFired:(NSTimer *)timer {
    self.currentIncrementingScore++;
    if (self.currentIncrementingScore > [ProgressionManager instance].currentScore) {
        [timer invalidate];
        self.scoreIncrementingTimer = nil;
    }
    else
    {
        self.uiLabelTotalScore.text = [NSString stringWithFormat:@"%i", self.currentIncrementingScore];
    }
}

//Getter for project title
-(NSString*) projectTitle
{
    return [@"Project " stringByAppendingString:[@(self.projectNumber) stringValue]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextLevelButtonTapped:(id)sender
{
    if (self.projectNumber > [ProgressionManager instance].highestLevelReached)
    {
        [self performSegueWithIdentifier:@"ScoreToPowerupSegue" sender:nil];
        [ProgressionManager instance].highestLevelReached = self.projectNumber;
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
