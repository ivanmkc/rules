//
//  ViewController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstructionSection.h"
#import "UIViewControllerWithDecoratedTileCollectionView.h"

@interface TutorialScreenViewController : UIViewControllerWithDecoratedTileCollectionView
@property (weak, nonatomic) InstructionSection *instructionSection;
@end
