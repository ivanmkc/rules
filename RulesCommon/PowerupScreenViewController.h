//
//  PowerupScreenViewController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-01.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnappingCollectionView.h"

@interface PowerupScreenViewController : UIViewController<SnappingCollectionViewDelegate>

@end
