//
//  ViewController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TutorialScreenViewController.h"
#import "TileFactory.h"
#import "TileCell.h"
#import "TileCollectionView.h"
#import "RulesSection.h"
#import "TileFactoryManager.h"
#import <Mixpanel/Mixpanel.h>

@interface TutorialScreenViewController ()

//@property (weak, nonatomic) IBOutlet UIButton *uiButtonNext;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelInstructions;
@property (weak, nonatomic) IBOutlet TileCollectionView *uiTileCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelRules;
@property (weak, nonatomic) IBOutlet UIView *uiViewDemoTile;

//@property (strong, nonatomic) StateController* stateController;
@end

@implementation TutorialScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.instructionSection isKindOfClass:[RulesSection class]])
    {
        RulesSection* instructionAndRuleSection = (RulesSection*) self.instructionSection;
        //Set rules in tile collection view
        self.uiTileCollectionView.numberOfTiles = instructionAndRuleSection.numberOfTiles;
        self.uiTileCollectionView.rules = instructionAndRuleSection.rules;
    }
    
    //Start tutorial
    [self startTutorial];
}

- (void) viewWillAppear:(BOOL)animated
{
    //Register nibs
    [self.uiTileCollectionView registerNib:[TileFactoryManager getNibInstance]
          forCellWithReuseIdentifier:@"TileCell"];
}

-(void)startTutorial
{
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Tutorial Attempted" properties:@{@"Tutorial Name" : self.instructionSection.instruction}];
    [mixpanel.people increment:@{
                                 @"Tutorial Attempted": @1
                                 }];
    
    [self showKnowledge];
}

-(void)showKnowledge
{
    //Set tutorial string
    if ([self.instructionSection isKindOfClass:[RulesSection class]])
    {
        self.uiLabelRules.text = [self.uiTileCollectionView getCurrentRule].information;
    }
    else if ([self.instructionSection isKindOfClass:[InstructionSection class]])
    {
        //Set tap recognizer
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(finishTutorial)];
        gestureRecognizer.numberOfTapsRequired = 1;
        gestureRecognizer.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:gestureRecognizer];
        
        self.uiLabelRules.text = @"Tap to continue...";
    }

//    self.uiLabelInstructions.text = self.instructionSection.instruction;
    
    //Set knowledge cell
//    if (self.instructionSection.tile)
//    {
//        TileCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CharacterTileCell" owner:self options:nil] firstObject];
//        [cell setTile: self.instructionSection.tile];
//        
//        cell.center = CGPointMake(self.uiViewDemoTile.frame.size.width/2, self.uiViewDemoTile.frame.size.height/2);
//        [self.uiViewDemoTile addSubview:cell];
//    }
}

-(void)receiveUserSelection
{
    //Hook up button to fire onLessonComplete
//    [self.uiButtonNext addTarget:self action:@selector(finishTutorial) forControlEvents:UIControlEventTouchUpInside];
}

-(void)finishTutorial
{
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Tutorial Completed" properties:@{@"Tutorial Name" : self.instructionSection.instruction}];
    [mixpanel.people increment:@{
                                 @"Tutorial Completed": @1
                                 }];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TileCollectionView delegates
- (void) onRuleReplaced:(Rule*) oldRule byNewRule:(Rule*) newRule
{
    self.uiLabelRules.text = newRule.information;
}

- (void) onAllRulesCompleted
{
    [self finishTutorial];
}

- (void)TileCollectionView:(TileCollectionView *)tileCollectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath
{
    
}
@end

