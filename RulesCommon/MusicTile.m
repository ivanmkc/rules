//
//  MusicTile.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicTile.h"
#import "Note.h"
#import "TileElement.h"

@implementation MusicTile

//Override music element setter to set image
-(void) setTileElement:(TileElement *) tileElement
{
    [super setTileElement:tileElement];

    //See if we have the image in the assets
    if ([tileElement isKindOfClass:[Note class]])
    {
        Note* note = (Note*) tileElement;
        NSString* noteImageName = [NSString stringWithFormat:@"%i_%i", note.midiValue, note.beatCount];

        [super setImage:[UIImage imageNamed:noteImageName]];
    }
}
@end
