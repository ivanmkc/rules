//
//  TileFactory.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileFactory.h"
#import "Tile.h"
#import "Rule.h"

@implementation TileFactory

-(NSArray*) makeTiles:(int)numberOfTiles
{
    NSArray* Tiles = [self makeTiles:numberOfTiles withRules:nil];
    return Tiles;
}

-(NSArray*) makeTiles:(int)numberOfTiles withRule:(id)rule
{
    NSArray* rules = [NSArray arrayWithObject:rule];
    NSArray* Tiles = [self makeTiles:numberOfTiles withRules:rules];
    return Tiles;
}

//Create  tiles
-(NSArray*) makeTiles:(int)numberOfTiles withRules:(NSArray *)rules
{
    NSMutableArray* tiles = [NSMutableArray array];
    
    //Add tiles for rules. Guarantee a minimum amount per rule
    if ((rules != nil)&&(rules.count>0))
    {
        //Get tile distribution
        NSArray* tileDistribution = [self makeTilesDistributionForNumberOfRules:rules.count andNumberOfTiles:numberOfTiles];
        
        for (int ruleIndex = 0; ruleIndex < rules.count; ruleIndex++)
        {
            Rule* rule = [rules objectAtIndex:ruleIndex];
            
            int numTilesAddedForRule = 0;
//            int minTilesPerRule = fmax(round(kRatioMinTilesPerRule*numberOfTiles), 1);
            int minTilesPerRule = [((NSNumber*) tileDistribution[ruleIndex]) integerValue];
            
            int numIterations = 0;
            while ((numTilesAddedForRule < minTilesPerRule)&&(tiles.count < numberOfTiles))
            {
                numIterations++;
                
                if (numIterations == 1000)
                {
                    NSLog(@"Warning: Rule ('%@') probably cannot be satisfied and caught in infinite loop.", rule.information);
                }
                
                Tile* tile = [self makeRandomTile];
                //See if the rule validates the tile
                if ([rule validatesTile:tile andUnpressedTiles:nil])
                {
                    BOOL alreadyValidated = NO;
                    //Check that it doesn't validate already validated rules again
                    for (int validatedRuleIndex=0; validatedRuleIndex<ruleIndex; validatedRuleIndex++)
                    {
                        Rule* validatedRule = [rules objectAtIndex:validatedRuleIndex];
                        if ([validatedRule validatesTile:tile andUnpressedTiles:nil])
                        {
                            alreadyValidated = YES;
                            break;
                        }
                    }
                    
                    if (!alreadyValidated)
                    {
                        [tiles addObject:tile];
                        numTilesAddedForRule++;
                    }
                }
            }
        }
    }
    
    //Add rest of tiles
    while (tiles.count < numberOfTiles)
    {
        Tile* tile = [self makeRandomTile];
        [tiles addObject:tile];
    }
    
    //Randomize array
    [self shuffleArray:tiles];
    
    return tiles;
}

//Create random tile
-(Tile*) makeRandomTile
{
    NSLog(@"Warning: This function must be implemented by subclasses");
    return nil;
}

//Generate probability distribution of tiles for rules
-(NSArray*) makeTilesDistributionForNumberOfRules:(int) numberOfRules andNumberOfTiles:(int) numberOfTiles
{
    NSMutableArray* tilesDistribution = [NSMutableArray arrayWithCapacity:numberOfRules];
    float remainingTiles = numberOfTiles;
    
    float probabilityRange = 0.3;
    float firstRuleFloor = 0.2;
    
    for (int ruleIndex = 0; ruleIndex < numberOfRules; ruleIndex++)
    {
        float probability = drand48()*probabilityRange;
        
        //If it's the first rule
        if (ruleIndex == 0)
        {
            probability += firstRuleFloor;
        }
        
        //Calculate the number of tiles for this rule
        int numberOfTilesForRule = fmin(remainingTiles, round(probability*numberOfTiles));
        //If it's the last rule
        if (ruleIndex == numberOfRules - 1)
        {
            numberOfTilesForRule = remainingTiles;
        }
            
        remainingTiles -= numberOfTilesForRule;
        
        tilesDistribution[ruleIndex] = @(numberOfTilesForRule);
    }
    
    return [NSArray arrayWithArray:tilesDistribution];
}

-(void)shuffleArray:(NSMutableArray*) array
{
    NSUInteger count = [array count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform(remainingCount);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

@end
