//
//  AnimatedTileCollectionView.m
//  Rules
//
//  Created by Ivan Cheung on 2014-09-25.
//  Copyright (c) 2014 Singspiel. All rights reserved.
//

#import "UIViewControllerWithDecoratedTileCollectionView.h"
#import "FISoundEngine.h"

@interface UIViewControllerWithDecoratedTileCollectionView()
//@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) NSMutableArray *animators;

@property (strong, nonatomic) FISound* correctSound;
@property (strong, nonatomic) FISound* incorrectSound;

@property BOOL isAnimated;
@end
@implementation UIViewControllerWithDecoratedTileCollectionView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Initialize animator
    self.animators = [NSMutableArray array];
//    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.isAnimated = YES;
    
    //Set up sounds
    FISoundEngine *engine = [FISoundEngine sharedEngine];
    
    //Initialize assets
    NSError* error;
    self.correctSound = [engine soundNamed:@"data/sounds/bubble.wav" maxPolyphony:4 error:&error];
    self.incorrectSound = [engine soundNamed:@"data/sounds/toaster.wav" maxPolyphony:4 error:&error];
}

- (void) onTileTapped:(TileCell*) tileCell wasCleared:(BOOL) wasCleared
{
    if (wasCleared)
    {
        //Play sound
        [self.correctSound play];
        
        if (self.isAnimated)
        {
            UIDynamicAnimator* animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
            //Add push
            UIPushBehavior* pushBehavior = [[UIPushBehavior alloc] initWithItems:@[tileCell] mode:UIPushBehaviorModeInstantaneous];
            
            pushBehavior.magnitude = 18.0f;
            pushBehavior.angle = (-M_PI/2);
            [animator addBehavior:pushBehavior];
            
            //Add gravity
            UIGravityBehavior* gravity = [[UIGravityBehavior alloc] initWithItems:@[tileCell]];
            gravity.magnitude = 10;
            [animator addBehavior:gravity];
            
            //Add angular velocity
            UIDynamicItemBehavior *itemBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[tileCell]];
            [itemBehaviour addAngularVelocity:-M_PI_2 forItem:tileCell];
            
            //Set the density so that the size of the tile doesn't matter
            float tileArea = tileCell.bounds.size.height*tileCell.bounds.size.width;
            [itemBehaviour setDensity:10000*1.0/tileArea];
            [animator addBehavior:itemBehaviour];
            
//            UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc]
//                                                      initWithItems:@[tileCell]];
//            collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
//            [animator addBehavior:collisionBehavior];
            
            [self.animators addObject:animator];
        }
        else
        {
            tileCell.hidden = YES;
        }
    }
    else
    {
        //Play sound
        [self.incorrectSound play];
        
        if (self.isAnimated)
        {
            //Shake the cell
            tileCell.transform = CGAffineTransformMakeRotation(-M_PI/8);
            // Animate
            [UIView animateWithDuration:0.5 delay:0
                 usingSpringWithDamping:0.2 initialSpringVelocity:20.0f
                                options:0 animations:^{
                                    tileCell.transform = CGAffineTransformMakeRotation(0);
                                    [self.view layoutIfNeeded];
                                } completion:nil];
        }
    }
}

-(void) onTileCreated:(TileCell *)tileCell forTile:(Tile *)tile
{
    if (self.isAnimated)
    {
        //Scale down first
        tileCell.transform = CGAffineTransformMakeScale(0.1, 0.1);
        //Scale up
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             tileCell.transform = CGAffineTransformIdentity;
                         }
                         completion:nil];
    }
    
    //TODO: Figure out why the shadow is being truncated
    CALayer* tileCellLayer = tileCell.uiImageViewBackground.layer;
    tileCellLayer.masksToBounds = NO;
    tileCellLayer.shadowColor = [UIColor blackColor].CGColor;
    tileCellLayer.shadowOffset = CGSizeMake(0.0f, 4.0f);
    tileCellLayer.shadowOpacity = 0.10f;
    tileCellLayer.shadowRadius = 0;
    
    //    CGRect rect = CGRectMake(0, 0, tileCell.bounds.size.width*0.8, tileCell.bounds.size.height*0.8);
    //    rect.origin.x = (tileCell.bounds.size.width - rect.size.width)/2;
    //    rect.origin.y = tileCell.bounds.size.height/10;
    //    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:5.0];
    //    tileCellLayer.shadowPath = shadowPath.CGPath;
}

-(void) onTilesReloaded
{
    for (UIDynamicAnimator* animator in self.animators)
    {
        [animator removeAllBehaviors];
    }
    
    [self.animators removeAllObjects];
//    NSArray* behaviors = [self.animator behaviors];
//    for (UIDynamicBehavior* behavior in behaviors)
//    {
//        [behavior de]
//    }
}
@end
