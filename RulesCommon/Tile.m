//
//  Tile.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Tile.h"

@implementation Tile
-(id) initWithTileElement:(TileElement*) element
{
    self = [super init];
    if (self)
    {
        self.tileElement = element;
    }
    
    return self;
}
@end
