//
//  Rule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "Rule.h"

@implementation Rule
-(id) initWithDescription:(NSString*) description
{
    self = [super init];
    
    if (self)
    {
        self.information = description;
    }

    return self;
}

-(BOOL) validatesTile:(Tile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    NSLog(@"Warning: This function must be implemented by subclasses");
    return YES;
}

@end
