//
//  Knowledge.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstructionSection.h"
#import "Rule.h"

@interface RulesSection : NSObject
@property int numberOfTiles;
@property (strong, nonatomic) NSArray* rules;

-(id)initWithRule:(Rule*) rule;
-(id)initWithRules:(NSArray*) rule;

@end
