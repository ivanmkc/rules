//
//  ExerciseScreenViewController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "ExerciseScreenViewController.h"
#import "TileFactoryManager.h"
#import "TileCell.h"
#import "TileCollectionView.h"

#import "PowerupManager.h"
#import "ProgressionManager.h"

#import <Mixpanel/Mixpanel.h>

#import "ScoreScreenViewController.h"

#import "Powerup.h"
#import "TimeModificationPowerup.h"

#import <Parse/Parse.h>

#import "RetryModalScreenView.h"

@class RulesSection;

@interface ExerciseScreenViewController ()

//@property (weak, nonatomic) IBOutlet UIButton *uiButtonNext;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsLayoutConstraintTitleTopSpace;

@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewRuby;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelCoins;

@property (weak, nonatomic) IBOutlet UILabel *uiLabelTimer;

@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewRulesFrame;

@property (strong, nonatomic) NSTimer *timer;
@property int secondsLeft;
//@property (strong, nonatomic) StateController* stateController;
@property (weak, nonatomic) TileFactory* tileFactory;

@property BOOL isShowingHints;
@property int score;

@property (strong, nonatomic) NSString* projectTitle;
@property (nonatomic) int secondsTotal;
@property (nonatomic) int baseSecondsTotal;

@property (strong, nonatomic) UITapGestureRecognizer * gestureRecognizer;

@property BOOL isExercisePassed;
@property BOOL isRetryingCurrentExercise;

@property (weak, nonatomic) RetryModalScreenView* retryScreen;
@end

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@implementation ExerciseScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    //Set pass switch
    self.isExercisePassed = NO;
    self.isRetryingCurrentExercise = NO;
    
    //If there are no rules, warn
    if (!self.instructionAndRuleSection.rules)
    {
        NSLog(@"Warning: There are no rules for this exercise.");
    }
    
    //Set tap recognizer
    self.gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(restartExercise)];
    self.gestureRecognizer.numberOfTapsRequired = 1;
    self.gestureRecognizer.numberOfTapsRequired = 1;
    
    [self.view addGestureRecognizer:self.gestureRecognizer];
    
    //Set rules in tile collection view
//    self.uiTileCollectionView.numberOfTiles = self.instructionAndRuleSection.numberOfTiles;
    self.uiTileCollectionView.rules = self.instructionAndRuleSection.rules;
    
    //Activate powerups
    self.secondsTotal = self.baseSecondsTotal;
    [self activatePowerups];
    
#if DEBUG
    [[ProgressionManager instance] addCoins:100];
#endif
    
    [PFPurchase addObserverForProduct:@"ca.studiozero.typo.coinpack"
                                    block:^(SKPaymentTransaction *transaction)
     {
         // Track buying coinpack
         Mixpanel *mixpanel = [Mixpanel sharedInstance];
         [mixpanel track:@"Purchased Coin Pack"];
         [mixpanel.people increment:@{
                                      @"Coin Packs Purchased": @1
                                      }];
         
         [[ProgressionManager instance] addCoins:10];
         self.uiLabelCoins.text = [@([ProgressionManager instance].getNumberOfCoins) stringValue];
          }];
    
    //Register nibs
    [self.uiTileCollectionView registerNib:[TileFactoryManager getNibInstance]
                forCellWithReuseIdentifier:@"TileCell"];
    
    if (IPAD)
    {
        self.uiLabelInstructions.font = [self.uiLabelInstructions.font fontWithSize:self.uiLabelInstructions.font.pointSize * 2 ];
        self.uiLabelRule.font = [self.uiLabelRule.font fontWithSize:self.uiLabelRule.font.pointSize * 2];
        self.uiLabelTitle.font = [self.uiLabelTitle.font fontWithSize:self.uiLabelTitle.font.pointSize * 2];
        self.uiLabelTimer.font = [self.uiLabelTimer.font fontWithSize:self.uiLabelTimer.font.pointSize * 2];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [self prepareExerciseTutorial];
}

- (void) viewDidAppear:(BOOL)animated
{
    //Start tutorial
    [self showTutorial];
}

- (void) viewWillDisappear:(BOOL)animated
{
    if ([self.timer isValid])
    {
        [self.timer invalidate];
    }
}

#pragma mark - Game parameters
//Getter for seconds total
-(int) baseSecondsTotal
{
    //Set timer parameters
#if DEBUG
    return 15;
#else
    return 15;
#endif
}

//Apply powerups
-(void)activatePowerups
{
//    NSArray* powerupTypes = [[PowerupManager instance] getPowerupTypes];
//    
//    for (NSString* powerupType in powerupTypes)
//    {
//        Powerup* powerup = [self getPurchasedPowerupForType:powerupType];
//        
//        if (powerup != nil)
//        {
//            //Active powerup
//            if ([powerup isKindOfClass:[TimeModificationPowerup class]])
//            {
////                TimeModificationPowerup* timePowerup = (TimeModificationPowerup*) powerup;
//                self.secondsTotal = (int)[[powerup activatePowerupOnParameter:@(self.baseSecondsTotal)] integerValue];
//            }
//        }
//    }
}

//Getter for project title
-(NSString*) projectTitle
{
    return [@"Project " stringByAppendingString:[@(self.projectNumber) stringValue]];
}

#pragma mark - Game logic
-(void)prepareExerciseTutorial
{
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Exercise Tutorial Attempted" properties:@{@"Exercise Tutorial Name" : self.projectTitle}];
    [mixpanel.people increment:@{
                                 @"Exercise Tutorials Attempted": @1
                                 }];
    
    //Hide tile view
    self.score = 0;
    self.uiTileCollectionView.hidden = YES;
    self.uiTileCollectionView.numberOfTiles = 3;
    
    self.isShowingHints = YES;
    
    //Set title
    self.uiLabelTitle.text = self.projectTitle;
    
    //Enable gesture recognizer
    self.gestureRecognizer.enabled = YES;
    
    self.uiTileCollectionView.alpha = 1;
    self.uiImageViewRulesFrame.alpha = 1;
    self.uiLabelRule.alpha = 1;
    
    //Hide coins
    self.uiImageViewRuby.hidden = YES;
    self.uiLabelCoins.hidden = YES;
    
    //Set instruction string
//    self.uiLabelInstructions.text = self.instructionAndRuleSection.instruction;
    self.uiLabelRule.text = [self.uiTileCollectionView getCurrentRule].information;
    self.uiLabelTimer.text = [NSString stringWithFormat:@"%d", self.secondsTotal];
}

-(void)showTutorial
{
}

-(void)finishExercise
{
    //Fire the rule changed delegate
    if ([self.delegate respondsToSelector:@selector(didFinishWithPassState:isRetryingCurrentExercise:)])
    {
        [self.delegate didFinishWithPassState:self.isExercisePassed isRetryingCurrentExercise:self.isRetryingCurrentExercise];
    }

    if (self.isExercisePassed)
    {
        [self.navigationController popViewControllerAnimated:YES];
//        [self performSegueWithIdentifier:@"ExerciseToScoreSegue" sender:@(self.score)];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons
- (void) didTapStartOver
{
    //Dismiss modal dialog
    [self.retryScreen dismiss];

    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"StartOver Attempted" properties:@{@"Exercise Number At StartOver" : @(self.projectNumber)}];
    [mixpanel.people increment:@{
                                 @"StartOvers Attempted": @1
                                 }];
    
    self.isExercisePassed = NO;
    self.isRetryingCurrentExercise = NO;
    [self finishExercise];
}

-(void) didTapRetry
{
    //Dismiss modal dialog
    [self.retryScreen dismiss];

    //Deduct from coins
    [[ProgressionManager instance] subtractCoins:3];
    self.uiLabelCoins.text = [@([ProgressionManager instance].getNumberOfCoins) stringValue];
    
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Retry Attempted" properties:@{@"Exercise Number At RetryFromHere" : @(self.projectNumber)}];
    [mixpanel.people increment:@{
                                 @"Retries Attempted": @1
                                 }];
    
    self.isRetryingCurrentExercise = YES;
    [self finishExercise];
}

- (void)restartExercise
{
    //Disable any gesture recognizers
    self.gestureRecognizer.enabled = NO;
    
    //Set tile settings
    self.uiTileCollectionView.numberOfTiles = self.instructionAndRuleSection.numberOfTiles;
    self.isShowingHints = NO;
    
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Exercise Attempted" properties:@{@"Exercise Number" : @(self.projectNumber)}];
    [mixpanel.people increment:@{
                                 @"Exercises Attempted": @1
                                 }];
    
    //Restart timer time
    self.secondsLeft = self.secondsTotal;
    
    //Reload tiles
    [self.uiTileCollectionView reloadTiles];
    
    self.uiTileCollectionView.hidden = NO;
    self.uiTileCollectionView.userInteractionEnabled = YES;
    
    //Start timer
    [self startTimer];
}

#pragma mark - Countdown timer
- (void)updateCounter:(NSTimer *) timer {
    if(self.secondsLeft > 0 ){
        self.secondsLeft -- ;
        self.uiLabelTimer.text = [NSString stringWithFormat:@"%d", self.secondsLeft];
    }
    else
    {
        //Fail
        self.uiTileCollectionView.userInteractionEnabled = NO;
        self.uiTileCollectionView.alpha = 0.25;
        self.uiImageViewRulesFrame.alpha = 0.25;
        self.uiLabelRule.alpha = 0.25;
        
        //Remove retry screen
//        [self.retryScreen popView];
//        [self performSegueWithIdentifier:@"ExerciseToRetrySegue" sender:nil];
        self.retryScreen = [RetryModalScreenView showInView:self.view];
        self.retryScreen.delegate = self;
        
//        self.uiButtonStartOver.hidden = NO;
//        self.uiViewRetry.hidden = self.projectNumber > 1 ? NO : YES;
        
        self.uiLabelInstructions.text = @"Time's up!";
        
        //Hide coins
        self.uiLabelCoins.text = [@([ProgressionManager instance].getNumberOfCoins) stringValue];
        self.uiImageViewRuby.hidden = NO;
        self.uiLabelCoins.hidden = NO;
        
        [self.timer invalidate];
    }
}

-(void)startTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}

#pragma mark - TileCollectionView delegates
- (void) onRuleReplaced:(Rule*)oldRule byNewRule:(Rule*) newRule
{
    if (oldRule != nil)
    {
        self.score += 30;
    }
    self.uiLabelRule.text = newRule.information;
}

- (void) onAllRulesCompleted
{
    self.score += 50;
    
    // Track opening lesson
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Exercise Completed" properties:@{@"Exercise Name" : @(self.projectNumber)}];
    [mixpanel.people increment:@{
                                 @"Exercises Completed": @1
                                 }];
    
    self.isExercisePassed = YES;
    
    //Finish exercise after delay
    [self.timer invalidate];
    [self performSelector:@selector(finishExercise) withObject:nil afterDelay:1];
}

- (void) onTileTapped:(TileCell*) tileCell wasCleared:(BOOL) wasCleared
{
    [super onTileTapped:tileCell wasCleared:wasCleared];
    if (wasCleared)
    {
        self.score += 10;
    }
    else
    {
    }
}

- (void) onTileCreated:(TileCell*)tileCell forTile:(Tile*) tile
{
    [super onTileCreated:tileCell forTile:tile];
    
//    CharacterTileCell* characterTileCell = (CharacterTileCell*) tileCell;
    tileCell.isShowingHints = self.isShowingHints;
    tileCell.isValidatedByRule = [[self.uiTileCollectionView getCurrentRule] validatesTile:tile andUnpressedTiles:nil];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ExerciseToScoreSegue"])
    {
        ScoreScreenViewController *viewController = segue.destinationViewController;
        viewController.projectNumber = self.projectNumber;
        viewController.secondsLeft = self.secondsLeft;
        viewController.secondsTotal = self.secondsTotal;
    }
    else if ([segue.identifier isEqualToString:@"ExerciseToRetrySegue"])
    {
        self.retryScreen = segue.destinationViewController;
        self.retryScreen.shouldShowRetryButton = self.projectNumber > 1 ? YES : NO;
        self.retryScreen.delegate = self;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

//#pragma mark - Countdown timer
//- (void)updateCounter:(NSTimer *) timer {
//    if(self.secondsLeft > 0 ){
//        self.secondsLeft -- ;
//        self.uiLabelTimer.text = [NSString stringWithFormat:@"%d", self.secondsLeft];
//    }
//    else
//    {
//        
//    }
//}
//
//-(void)startTimer{
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
//}


@end
