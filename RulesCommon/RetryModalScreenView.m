//
//  RetryModalScreenViewController.m
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-11-04.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import "RetryModalScreenView.h"
#import "ProgressionManager.h"
#import "PendingActivityModalView.h"
#import <Parse/Parse.h>

@interface RetryModalScreenView ()
@property (weak, nonatomic) IBOutlet UILabel *uiLabelRetryCost;
@property (weak, nonatomic) IBOutlet UIButton *uiButtonRetry;
@property (weak, nonatomic) IBOutlet UIButton *uiButtonStartOver;
@end

static const int kRetryCost = 3;

@implementation RetryModalScreenView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //Set label to show retry cost
        self.uiLabelRetryCost.text = [NSString stringWithFormat:@"x%i", kRetryCost];
    }
    return self;
}

-(void) didMoveToWindow
{
    [UIView animateWithDuration:0.5
          delay:0
        options:UIViewAnimationOptionCurveEaseOut
         animations:^{
            self.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0 alpha:0.3];
         } completion:^(BOOL finished) {
//             <#code#>
         }];

}

+ (RetryModalScreenView *) showInView: (UIView *) parentView
{
    RetryModalScreenView * view = [[[NSBundle mainBundle] loadNibNamed: @"RetryModalScreenView" owner:self options:nil ]firstObject ];
    
    view.frame = parentView.frame;
    
    [parentView addSubview: view];
    
    return view;
}

- (void) dismiss
{
    [self removeFromSuperview];
}

- (IBAction)didTapRetryButton:(id)sender {
    if ([ProgressionManager instance].getNumberOfCoins >= kRetryCost)
    {
        if ([self.delegate respondsToSelector:@selector(didTapRetry)])
        {
            [self.delegate didTapRetry];
        }
    }
    else
    {
        PendingActivityModalView * modalBuyingScreen = [PendingActivityModalView showInView: self
                                                                                   withText: @"Connecting to the AppStore"];
        //Show purchase dialog
        [PFPurchase buyProduct:@"ca.studiozero.typo.coinpack" block:^(NSError *error)
         {
             [modalBuyingScreen dismiss];
             if (error)
             {
                 [[[UIAlertView alloc] initWithTitle:@"Purchase Error"
                                             message:error.localizedDescription
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
             }
         }];
    }
}

- (IBAction)didTapStartOverButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didTapStartOver)])
    {
        [self.delegate didTapStartOver];
    }
}

-(void) setShouldShowRetryButton:(BOOL)shouldShowRetryButton
{
    _shouldShowRetryButton = shouldShowRetryButton;
    self.uiButtonRetry.hidden = !shouldShowRetryButton;
}
@end
