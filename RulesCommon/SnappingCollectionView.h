//
//  SnappingCollectionView.h
//  iOSAudio
//
//  Created by Ivan Cheung on 2014-06-03.
//  Copyright (c) 2014 George Tam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SnappingCollectionView;
@protocol SnappingCollectionViewDelegate <NSObject>
    @optional
    - (void)snappingCollectionView:(SnappingCollectionView *)snappingCollectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath;
    - (void)snappingCollectionView:(SnappingCollectionView *)snappingCollectionView didSnapToIndexPath:(NSIndexPath*)indexPath;
@end

@interface SnappingCollectionView : UICollectionView<UIScrollViewDelegate, UICollectionViewDelegateFlowLayout>
@end