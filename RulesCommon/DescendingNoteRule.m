//
//  DescendingNoteRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "DescendingNoteRule.h"
#import "Note.h"

@implementation DescendingNoteRule

-(BOOL) validatesMusicTile:(MusicTile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    if ([tile.tileElement isKindOfClass:[Note class]])
    {
        Note* note = (Note*) tile.tileElement;
        int maxMidiValue = 0;
        //Get max note value for unpressed tiles
        for (MusicTile* unpressedTile in unpressedTiles)
        {
            Note* unpressedTileNote = (Note*) unpressedTile.tileElement;
            
            if (unpressedTileNote.midiValue > maxMidiValue)
            {
                maxMidiValue = unpressedTileNote.midiValue;
            }
        }
        
        return (note.midiValue >= maxMidiValue);
    }
    else
    {
        return NO;
    }
}

@end
