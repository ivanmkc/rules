
//
//  InstructionAndContinueSection.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-24.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "InstructionSection.h"

@implementation InstructionSection

-(id)initWithInstruction:(NSString*) instruction
{
    self = [super init];
    if (self)
    {
        self.instruction = instruction;
    }
    return self;
}
@end
