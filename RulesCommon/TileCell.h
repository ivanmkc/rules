//
//  MusicTileCellController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tile.h"

@interface TileCell : UICollectionViewCell
@property (weak, nonatomic) Tile* tile;
@property (nonatomic) BOOL isPressed;

@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewBackground;

@property (weak, nonatomic) IBOutlet UILabel *uiLabelName;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageViewImage;

@property (nonatomic) BOOL isShowingHints;
@property (nonatomic) BOOL isValidatedByRule;
@end
