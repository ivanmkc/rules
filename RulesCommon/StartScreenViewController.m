 //
//  StartScreenViewController.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "StartScreenViewController.h"
#import "TileSetSwitcherSection.h"
#import "RulesSection.h"
#import "TutorialScreenViewController.h"

//Note Rules
//#import "NoteRule.h"
//#import "DescendingNoteRule.h"
//#import "LineOrSpaceNoteRule.h"

//TileFactories
#import "TileFactoryManager.h"

//Powerups
#import "PowerupManager.h"
#import "Powerup.h"
#import "TimeModificationPowerup.h"

#import "ProgressionManager.h"

@interface StartScreenViewController ()
@property int currentSectionIndex;
@property BOOL isLastSectionPassed;
@property BOOL isRetryingLastSection;
@property int numberOfSwitchersActivated;
@end

static const int kCoinsStart = 5;

@implementation StartScreenViewController

@synthesize sections = _sections;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.sections = [NSMutableArray array];
        self.isLastSectionPassed = YES;
        self.isRetryingLastSection = NO;
        self.numberOfSwitchersActivated = 0;
        self.sections = nil;
   
        //Add powerups
        //Pencil type
        [[PowerupManager instance] addPowerup:[[TimeModificationPowerup alloc] initWithName:@"Pencil" andDescription:@"Basics are best." andEffectDescription:@"+2 seconds" andCost:200 andTimeIncrease:3]];
//        [[PowerupManager instance] addPowerup:[[TimeModificationPowerup alloc] initWithName:@"Pen" andDescription:@"Never need to sharpen this." andEffectDescription:@"+5 seconds" andCost:300 andTimeIncrease:8]];
        
        [[PowerupManager instance] addPowerup:[[Powerup alloc] initWithName:@"Glasses" andDescription:@"See fonts in different colors." andCost:100]];
        
//        [[PowerupManager instance] addPowerup:[[Powerup alloc] initWithName:@"Colorchart" andDescription:@"Pretty colors!" andCost:100]];
        
        [[PowerupManager instance] addPowerup:[[Powerup alloc] initWithName:@"Eraser" andDescription:@"Erase a couple of tiles!" andCost:0]];
        
        [[PowerupManager instance] addPowerup:[[Powerup alloc] initWithName:@"Ruby" andDescription:@"Use this to buy powerups." andCost:0]];
        
        [[PowerupManager instance] addPowerup:[[Powerup alloc] initWithName:@"3 Erasers" andDescription:@"Erases tiles." andCost:1]];
        
        //Skip tutorial if already completed
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasCompletedTutorial"]) {
            self.currentSectionIndex = 0;
        }
        else
        {
            self.currentSectionIndex = self.exerciseStartIndex;
        }
    }
    
    return self;
}

//Subclasses should override this. TODO: Make this a protocol
-(NSArray*) sections
{
    return _sections;
}

//Subclasses should override this. TODO: Make this a protocol
-(int) exerciseStartIndex
{
    return 3;
}

-(void) setSections:(NSArray *)sections
{
    _sections = sections;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    //TODO: Hide status bar
    
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    //If not a pass, start over
    if (!self.isLastSectionPassed)
    {
        if (!self.isRetryingLastSection)
        {
            self.currentSectionIndex = self.exerciseStartIndex;
            self.numberOfSwitchersActivated = 0;
        }
        else
        {
            //Reset to last section attempted
            self.currentSectionIndex--;
        }
    }
    
    [self loadNextSection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)loadNextSection
{
    //Load next section
    if (self.currentSectionIndex < self.sections.count)
    {
        NSObject* section = [self.sections objectAtIndex:self.currentSectionIndex];
        
        //Increment section index
        self.currentSectionIndex++;
        
        if (([section isKindOfClass:[InstructionSection class]])&&(self.currentSectionIndex-1 < self.exerciseStartIndex))
        {
            [self performSegueWithIdentifier:@"StartToTutorialSegue" sender:section];
        }
        else if ([section isKindOfClass:[RulesSection class]])
        {
            // Save that tutorial is completed
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasCompletedTutorial"]) {
                //Save locally
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasCompletedTutorial"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //Add 10 coins
                [[ProgressionManager instance] addCoins:kCoinsStart];
            }
            
            [self performSegueWithIdentifier:@"StartToExerciseSegue" sender:section];
        }
        else if ([section isKindOfClass:[TileSetSwitcherSection class]])
        {
            self.numberOfSwitchersActivated++;
            TileSetSwitcherSection* tileSetSwitcher = (TileSetSwitcherSection*) section;
            TileFactory* tileFactory = [TileFactoryManager getInstance];
            tileFactory.tileSet = tileSetSwitcher.tileSet;
            [self loadNextSection];
        }
    }
    else if (self.currentSectionIndex == self.sections.count)
    {
        [self performSegueWithIdentifier:@"StartToFinishSegue" sender:nil];
        
        //Return to start
        self.currentSectionIndex = self.exerciseStartIndex;
        self.numberOfSwitchersActivated = 0;
    }
}

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        if ([segue.identifier isEqualToString:@"StartToTutorialSegue"])
        {
            TutorialScreenViewController *viewController = segue.destinationViewController;
            viewController.instructionSection = (InstructionSection*) sender;
        }
        else if ([segue.identifier isEqualToString:@"StartToExerciseSegue"])
        {
            ExerciseScreenViewController *viewController = segue.destinationViewController;
            viewController.instructionAndRuleSection = (RulesSection*) sender;
            viewController.projectNumber = self.currentSectionIndex - self.exerciseStartIndex - self.numberOfSwitchersActivated;
            viewController.delegate = self;
        }
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    
    #pragma mark - TileCollectionView delegates
    - (void)didFinishWithPassState:(BOOL)isPassed isRetryingCurrentExercise:(BOOL)isRetrying
    {
        self.isLastSectionPassed = isPassed;
        self.isRetryingLastSection = isRetrying;
    }
@end
