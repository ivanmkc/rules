//
//  TileFactory.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-13.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rule.h"

@interface TileFactory : NSObject
-(NSArray*) makeTiles:(int)numberOfTiles;
-(NSArray*) makeTiles:(int)numberOfTiles withRule:(Rule*)rule;
-(NSArray*) makeTiles:(int)numberOfTiles withRules:(NSArray*)rules;

@property int tileSet;
@end
