//
//  MusicRule.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicRule.h"
#import "Note.h"

@implementation MusicRule

-(BOOL) validatesTile:(Tile*) tile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    MusicTile* musicTile = (MusicTile*) tile;
   return [self validatesMusicTile:musicTile andUnpressedTiles:unpressedTiles];
}

-(BOOL) validatesMusicTile:(MusicTile*) musicTile andUnpressedTiles:(NSArray*) unpressedTiles;
{
    NSLog(@"Warning: This function must be implemented by subclasses");
    if ([musicTile isKindOfClass:[MusicTile class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
