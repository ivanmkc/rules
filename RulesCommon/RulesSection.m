//
//  Knowledge.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "RulesSection.h"

@implementation RulesSection

static const int kDefaultNumberOfTiles = 3;

-(id)initWithRule:(Rule *)rule
{
    NSArray* rules = [NSArray arrayWithObject:rule];
    self = [self initWithRules:rules];
    
    return self;
}

-(id)initWithRules:(NSArray *)rules
{
    self = [super init];
    if (self)
    {
        //Clone the provided rules
        self.rules = [NSArray arrayWithArray:rules];
        self.numberOfTiles = kDefaultNumberOfTiles;
    }
    return self;
}

@end
