//
//  AnimatedTileCollectionView.h
//  Rules
//
//  Created by Ivan Cheung on 2014-09-25.
//  Copyright (c) 2014 Singspiel. All rights reserved.
//

#import "TileCollectionView.h"
#import <AVFoundation/AVFoundation.h>
#import "RetryModalScreenView.h"

@interface UIViewControllerWithDecoratedTileCollectionView : UIViewController<TileCollectionViewDelegate, AVAudioPlayerDelegate, RetryModalScreenViewControllerDelegate>

@end
