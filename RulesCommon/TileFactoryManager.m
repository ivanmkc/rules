//
//  FactoryManager.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-19.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "TileFactoryManager.h"
#import "TileFactory.h"
#import "Rule.h"

//#import "MusicRule.h"
//#import "MusicTileFactory.h"
//
//#import "CharacterRule.h"
//#import "CharacterTileFactory.h"

//Dictionary to store subclass instances
//static NSMutableDictionary *sInstances;
static TileFactory* sInstance;
static UINib* sNibInstance;

@implementation TileFactoryManager

+(TileFactory*) getInstance
{
    NSAssert(sInstance != nil, @"TileFactoryManager has not been set!");
    return sInstance;
}

+(void) setInstance:(TileFactory*) tileFactory
{
    sInstance = tileFactory;
}

+(UINib*) getNibInstance
{
    NSAssert(sNibInstance != nil, @"TileFactoryManager Nib has not been set!");
    return sNibInstance;
}

+(void) setNibInstance:(UINib*) nib
{
    sNibInstance = nib;
}

//+(TileFactory*) tileFactoryInstanceForClass:(Class) tileFactoryClass
//{
//    //Make instances dictionary
//    if (!sInstances) {
//        sInstances = [[NSMutableDictionary alloc] init];
//    }
//    
//    //Get the saved instance
//    TileFactory* instance = [sInstances objectForKey:NSStringFromClass(tileFactoryClass)];
//    
//    //If the instance doesn't exist, make it
//    if (!instance) {
//        instance = [[tileFactoryClass alloc] init];
//        [sInstances setObject:instance forKey:NSStringFromClass(tileFactoryClass)];
//    }
//    
//    return instance;
//}
//
//+(TileFactory*) tileFactoryInstanceForRule:(Rule*) rule
//{
//    Class tileFactoryClass = nil;
//    if ([rule isKindOfClass:[MusicRule class]])
//    {
//        tileFactoryClass = [MusicTileFactory class];
//    }
//    else if ([rule isKindOfClass:[CharacterRule class]])
//    {
//        tileFactoryClass = [CharacterTileFactory class];
//    }
//    else
//    {
//        return nil;
//    }
//    
//    return [self tileFactoryInstanceForClass:tileFactoryClass];
//}
@end
