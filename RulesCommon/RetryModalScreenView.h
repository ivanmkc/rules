//
//  RetryModalScreenViewController.h
//  Food Tiles
//
//  Created by Ivan Cheung on 2014-11-04.
//  Copyright (c) 2014 Studio 0. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RetryModalScreenViewControllerDelegate <NSObject>
@optional
- (void)didTapRetry;
- (void)didTapStartOver;
@end

@interface RetryModalScreenView : UIView
@property (weak, nonatomic) id<RetryModalScreenViewControllerDelegate> delegate;
@property (nonatomic) BOOL shouldShowRetryButton;

+ (RetryModalScreenView *) showInView: (UIView *) parentView;
- (void) dismiss;
@end
