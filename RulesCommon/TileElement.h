//
//  TileElement.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TileElement : NSObject
@property (strong, nonatomic) NSString* information;
@end
