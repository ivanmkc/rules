//
//  TileSetSwitch.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-22.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TileSetSwitcherSection : NSObject
- (id) initWithTargetTileSet:(int) targetTileSet forTileFactoryClass:(Class) tileFactoryClass;

@property Class tileFactoryClass;
@property int tileSet;
@end
