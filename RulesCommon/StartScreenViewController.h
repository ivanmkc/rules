//
//  StartScreenViewController.h
//  Rules
//
//  Created by Ivan Cheung on 2014-08-12.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExerciseScreenViewController.h"
@interface StartScreenViewController : UIViewController <ExerciseScreenViewControllerDelegate>
@property (strong, nonatomic) NSArray* sections;
@property (readonly, nonatomic) int exerciseStartIndex;
@end
