//
//  MusicTileFactory.m
//  Rules
//
//  Created by Ivan Cheung on 2014-08-17.
//  Copyright (c) 2014 Ivan Cheung. All rights reserved.
//

#import "MusicTileFactory.h"
#import "MusicTile.h"
#import "Note.h"

@implementation MusicTileFactory

//Create random music tile
-(Tile*) makeRandomTile
{
    NSArray* NOTES_SIMPLE = @[@60, @62, @64, @65, @67, @69, @71];
    
    //Roll a die to generate a random tile
    UInt32 noteNumberIndex = arc4random()%(NOTES_SIMPLE.count);
    int noteNumber = [[NOTES_SIMPLE objectAtIndex:noteNumberIndex] integerValue];
    
    Note* note = [[Note alloc] initWithMidiValue:noteNumber andBeatCount:1];
    MusicTile* tile = [[MusicTile alloc] initWithTileElement:note];
    tile.tileElement = note;
    return tile;
}
@end
